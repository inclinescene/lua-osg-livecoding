// lua2.cpp: Lua2 interface for OpenSceneGraph
// Copyright (c) 2018 cxw/Incline.  OSGPL.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

// Contains code from LuaScriptEngine.cpp - original copyright at end

/**
 * \file lua2.cpp
 * \brief Lua2 interface for OpenSceneGraph
 *
 * \details
 * This file implements an alternative interface to the OSG LuaScriptEngine.
 * The implementation is unchanged, but the property access style is different.
 *
 * - Member access:
 *   - The existing LuaScriptEngine ("LSE") interface uses `__index` and
 *     `__newindex` functions to access properties and methods.  It does not
 *     use metatables.  As a result, the properties and methods are not visible
 *     from Lua code.
 *   - Lua2 uses chained, single-inheritance metatables to store properties
 *     and methods.  As a result, all the properties and methods are directly
 *     visible to Lua code.
 *   - Lua2 uses a function-based style: `obj:Property()` is a getter, and
 *     `obj:Property(new_value)` is the corresponding setter.
 *
 * - Error handling:
 *   - LSE generally returns `nil` and outputs a message on OSG_NOTICE in
 *     case of errors.
 *   - Lua2 uses luaL_error() to report errors.  This makes the details
 *     of the errors visible to Lua code in a way they are not with LSE.
 *
 */

#include "bulk.hpp"
#include "lua2.h"

/// A Lua function that gives you a place to put a C++ breakpoint.
/// The breakpoint should go at lua2.cpp:42 (easy to remember!).
static int cxx_bp(lua_State *)
{   volatile int i = 0;
    ++i;            // Put a breakpoint here if you want one.  Line 42 so it's
    return 0;       // easy to remember.
}

#include <osgDB/ClassInterface>
#include <osgDB/ObjectWrapper>

using namespace osg;
using namespace std;
using namespace OpenThreads;

// Lua experiments //////////////////////////////////////////////////// {{{1

/**
 * \file lua2.cpp
 *
 * \details The main entry point is registerCFunctions(), which sets up the
 * Lua2 system given a LuaScriptEngine.  The primary Lua global function it
 * registers is `makeLua2Object()`, implemented by CF_makeLua2Object().
 * `makeLua2Object()` returns the Lua2 object given an LSE object.
 */

// Metatable and field names
#define LUA2_UNREF_MT       ("Lua2.UnrefObject")
#define LSE_OBJ_PTR         ("object_ptr")
#define LUA2_OBJ_PTR        ("osgObject")
#define LUA2_TOMTS          ("Lua2.TableOfMetaTables")
#define LUA2_MT_CLASSNAME   ("Lua2.MT.compoundClassName")
#define LUA2_ROOT_METATABLE ("Lua2.RootMetaTable")
    ///< The metatable with dir() in it

/// Name of an osg userdata item storing the registry index of an lval() value
#define LUA2_TBL_USERVAL_NAME   ("__LUA2__TBL_USERVALUE")

/// A handy ClassInterface
static osgDB::ClassInterface _ci;

// Forward declarations of Lua CFunctions
static int CF_UnitaryPropertyAccessor(lua_State *_lua);
static int CF_MethodRunner(lua_State *_lua);
static int CF_makeLua2Object(lua_State *_lua);
static int CF_getObjectPtr(lua_State * _lua);
static int CF_DirLua2(lua_State *_lua);
static int CF_LuaValueAccessor(lua_State *_lua);
static int CF_UnitaryPropertyAccessor(lua_State *_lua);
static int CF_garbageCollectObject(lua_State* _lua);

// "LSE" refers to the way LuaScriptEngine exposes osg objects to Lua.
// "Lua2" refers to the way this code exposes osg objects to Lua.
// This code licensed OSGPL.  Some code from LuaScriptEngine.{h,cpp}.

/// Get or make a Lua metatable for the given class.
/// Leaves that metatable on top of the stack.
void pushMetatableForClass(const lua::LuaScriptEngine *lse, const std::string compoundClassName)
{
    lua_State *_lua = lse->getLuaState();

    lua_pushstring(_lua, LUA2_TOMTS);           // K ]
    lua_gettable(_lua, LUA_REGISTRYINDEX);      // ToMTs ]

    // Create the table of metatables if necessary
    if(lua_type(_lua, -1) != LUA_TTABLE) {
        lua_pop(_lua, 1);                       // ]

        // Make the table of metatables and save it in the registry
        lua_newtable(_lua);                     // ToMTs ]
        lua_pushstring(_lua, LUA2_TOMTS);       // ToMTs K ]
        lua_pushvalue(_lua, -2);                // ToMTs K ToMTs ]
        lua_settable(_lua, LUA_REGISTRYINDEX);  // ToMTs ]
    }

    // See if our class is in the metatable
    lua_pushstring(_lua, compoundClassName.c_str());    // ToMTs K ]
    lua_gettable(_lua, -2);                             // ToMTs MT ]

    // Create the metatable if necessary
    if(lua_type(_lua, -1) != LUA_TTABLE) {
        OSG_INFO << "Creating Lua2 metatable for " << compoundClassName << endl;

        lua_pop(_lua, 1);                                   // ToMTs ]
        lua_newtable(_lua);                                 // ToMTs MT ]

        // Add the class name to the metatable
        lua_pushstring(_lua, LUA2_MT_CLASSNAME);            // ToMTs MT K ]
        lua_pushstring(_lua, compoundClassName.c_str());    // ToMTs MT K V ]
        lua_settable(_lua, -3);                             // ToMTs MT ]

        // Set up this metatable to be used for property lookups
        lua_pushstring(_lua, "__index");            // ToMTs MT K ]
        lua_pushvalue(_lua, -2);                    // ToMTs MT K MT ]
        lua_settable(_lua, -3);                     // ToMTs MT ]

        // Add accessors for the properties of this class
        {
            osgDB::ClassInterface::PropertyMap properties;
            if(_ci.getSupportedProperties(compoundClassName, properties, false)) {
                // false => no associates

                for(osgDB::ClassInterface::PropertyMap::const_iterator
                        citr = properties.cbegin();
                    citr != properties.cend();
                    ++citr
                ) {
                    osgDB::BaseSerializer::Type ty = citr->second;
                    if( (ty == osgDB::BaseSerializer::RW_VECTOR) ||
                        (ty == osgDB::BaseSerializer::RW_MAP) ||
                        (ty == osgDB::BaseSerializer::RW_UNDEFINED) ||
                        (ty == osgDB::BaseSerializer::RW_USER)
                    ) { // TODO update this when I support vectors and maps
                        OSG_WARN << "Skipping property " << citr->first << " of class "
                            << compoundClassName << " and type "
                            << _ci.getTypeName(ty) << endl;
                        continue;
                    }

                    lua_pushstring(_lua, citr->first.c_str());    // ToMTs MT propname ]

                    // Make the closure for the accessor
                    lua_pushlightuserdata(_lua, const_cast<lua::LuaScriptEngine*>(lse));
                                                        // ToMTs MT propname lse ]
                    lua_pushvalue(_lua, -2);            // ToMTs MT propname lse propname ]
                    lua_pushcclosure(_lua, CF_UnitaryPropertyAccessor, 2);
                                                        // ToMTs MT propname closure ]

                    lua_settable(_lua, -3);             // ToMTs MT ]
                } //foreach property
            } //endif has properties
        }

        // Add the methods
        {
            osgDB::ClassInterface::PropertyMap methods;
            if(_ci.getSupportedMethods(compoundClassName, methods, false)) {
                for(osgDB::ClassInterface::PropertyMap::const_iterator
                        citr = methods.cbegin();
                    citr != methods.cend();
                    ++citr
                ) {
                    lua_pushstring(_lua, citr->first.c_str());    // ToMTs MT methname ]

                    // Make the closure for the accessor
                    lua_pushlightuserdata(_lua, const_cast<lua::LuaScriptEngine*>(lse));
                                                        // ToMTs MT methname lse ]
                    lua_pushvalue(_lua, -2);            // ToMTs MT methname lse methname ]
                    lua_pushcclosure(_lua, CF_MethodRunner, 2);
                                                        // ToMTs MT methname closure ]

                    lua_settable(_lua, -3);             // ToMTs MT ]
                } //foreach method

            } //endif has methods
        }

        // Set this table's metatable to the parent class's metatable,
        // if there is one.
        bool associated_osg_Object(compoundClassName == "osg::Object");
            ///< whether osg::Object is an associate

        osg::ref_ptr<osgDB::ObjectWrapper> ow(_ci.getObjectWrapper(compoundClassName));
        osgDB::ObjectWrapper::RevisionAssociateList associates;

        if(ow.valid()) {
            associates = ow->getAssociates();

            osgDB::ObjectWrapper::RevisionAssociateList::const_reverse_iterator aitr(associates.crbegin());

            // Usually the class itself is the last thing in the associate list.
            // Skip it, if so.
            if(aitr != associates.crend() && aitr->_name == compoundClassName) {
                ++aitr;
            }

            if(aitr != associates.crend()) {     // We have a parent
                pushMetatableForClass(lse, aitr->_name);    // ToMTs MT MT2 ]
                lua_setmetatable(_lua, -2);                 // ToMTs MT ]
            }

            // Check for an osg::Object association
            for( ; aitr != associates.crend(); ++aitr) {
                if(aitr->_name == "osg::Object") {
                    associated_osg_Object = true;
                    break;
                }
            }

        } //endif have ObjectWrapper

        if(!associated_osg_Object) {
            // No wrapper or otherwise won't inherit from osg::Object via
            // the associate chain, so hook it into the root metatable
            // so it will still have ptr() and a dir().
            lua_getglobal(_lua, LUA2_ROOT_METATABLE);       // ToMTs MT RootMT ]
            lua_setmetatable(_lua, -2);                     // ToMTs MT ]
        }

        // Add the metatable to the table of metatables
        lua_pushstring(_lua, compoundClassName.c_str());    // ToMTs MT K ]
        lua_pushvalue(_lua, -2);                            // ToMTs MT K MT ]
        lua_settable(_lua, -4);                             // ToMTs MT ]
    }

    lua_remove(_lua, -2);                       // MT ]
} //pushMetatableForClass

/// Get the osg::Object from an LSE or Lua2 instance
osg::ref_ptr<osg::Object> getObjectFromStack(const lua::LuaScriptEngine *lse,
        int pos, bool isLSE = false, bool quiet = false)
{
    osg::ref_ptr<osg::Object> retval(0);
    lua_State *_lua = lse->getLuaState();
    pos = lse->getAbsolutePos(pos);

    if (lua_type(_lua, pos)!=LUA_TTABLE) {          // ... T ... ]
        OSG_WARN << "Param is not a table" << endl;
    } else {
        lua_pushstring(_lua, isLSE ? LSE_OBJ_PTR : LUA2_OBJ_PTR);
                                                    // ... T ... str ]
        lua_rawget(_lua, pos);                      // ... T ... val ]

        if (lua_type(_lua, -1)!=LUA_TUSERDATA) {
            if(!quiet) OSG_WARN << (isLSE ? "LSE_OBJ_PTR" : "LUA2_OBJ_PTR")
                    << " is not a userdata" << endl;
            lua_pop(_lua, 1);       // pop whatever it was  // ... T ... ]

        } else {
            osg::Object *object = *const_cast<osg::Object**>(reinterpret_cast<const osg::Object**>(lua_touserdata(_lua,-1)));
            lua_pop(_lua, 1);       // pop the userdata     // ... T ... ]
            retval = object;
        }
    }

    return retval;
} //getObjectFromStack

/// Make a Lua2 object instance given an osg::Object, and leave it on the stack.
/// Push an nil if object==0.
void pushLua2Object(const lua::LuaScriptEngine *lse, osg::Object *object)
{
    lua_State *_lua = lse->getLuaState();

    if(!object) {
        lua_pushnil(_lua);
        return;
    }

    // Make the new object
    std::string compoundName = object->getCompoundClassName();
    lua_newtable(_lua);                                         // T ]

    // set up object_ptr to handle ref/unref of the object
    {
        lua_pushstring(_lua, LUA2_OBJ_PTR);                     // T K ]

        // create user data for pointer
        void* userdata = lua_newuserdata( _lua, sizeof(osg::Object*));
                                                                // T K U ]
        (*reinterpret_cast<osg::Object**>(userdata)) = object;

        luaL_getmetatable( _lua, LUA2_UNREF_MT);                // T K U MT ]
        lua_setmetatable( _lua, -2 );                           // T K U ]

        lua_settable(_lua, -3);                                 // T ]

        // increment the reference count as Lua now will unreference
        // it once it's finished with the userdata for the pointer
        object->ref();
    }

    // Basic fields
    lua_pushstring(_lua, "libraryName"); lua_pushstring(_lua, object->libraryName()); lua_settable(_lua, -3);
    lua_pushstring(_lua, "className"); lua_pushstring(_lua, object->className()); lua_settable(_lua, -3);
    lua_pushstring(_lua, "compoundClassName"); lua_pushstring(_lua, object->getCompoundClassName().c_str()); lua_settable(_lua, -3);

    // Set the metatable for this class
    pushMetatableForClass(lse, object->getCompoundClassName());     // T MT ]
    lua_setmetatable(_lua, -2);                                     // T ]

} //pushLua2Object

/// Set or get a single-valued (non-vector, non-map) property.
/// This property can be a callback object / Lua function.
/// Upvalues: 1 = LSE; 2 = name of the property
static int CF_UnitaryPropertyAccessor(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));
    const std::string propertyName(lua_tostring(_lua, lua_upvalueindex(2)));

    int n = lua_gettop(_lua);    /* number of arguments */
    if( (n<1) || (n>2) ) {
        std::string errmsg("Call as <instance>:");
        errmsg += propertyName;
        errmsg += "([new value])";
        return luaL_error(_lua, errmsg.c_str());
    }

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));
    if(!object.valid()) {
        return luaL_error(_lua, "Parameter 1 is not a Lua2 instance");
    }

    if(n == 1) {    // Getter                           // Lua2 ]
        int retval = lse->pushPropertyToStack(object, propertyName);
        if(retval == 1) {
            if(lua_type(_lua, -1) == LUA_TTABLE) {
                // Get the LSE-format object, if the result is one
                osg::ref_ptr<osg::Object> newobject(
                        getObjectFromStack(lse, -1, true, true));

                if(newobject.valid()) {
                    lua_pop(_lua, 1);       // Remove the LSE-format object
                    pushLua2Object(lse, newobject); // Replace it with Lua2
                        // This is slow but DRY.
                }
                // If !newobject.valid(), the top of the stack isn't an LSE
                // object.  Therefore, leave it alone.
            }
        }
        return retval;
    }

    // Setter                                           // Lua2 newval ]
    osg::ref_ptr<osg::Object> paramobject(getObjectFromStack(lse, -1, false, true));
    if(paramobject.valid()) {   // If it's a Lua2-format object...
        lua_pop(_lua, 1);       // Remove the Lua2-format object    // Lua2 ]
        lse->pushObject(paramobject);   // Replace it with LSE      // Lua2 LSEval ]
            // Also slow but DRY.
    } //otherwise leave it as it is.

    return lse->setPropertyFromStack(object, propertyName);
} //CF_UnitaryPropertyAccessor

/// Invoke a method.
/// Upvalues: 1 = LSE; 2 = name of the method
/// Modified from LuaScriptEngine.cpp:callClassMethod().
static int CF_MethodRunner(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));
    const std::string methodName(lua_tostring(_lua, lua_upvalueindex(2)));

    int n = lua_gettop(_lua);    /* number of arguments */
    if(n<1) {
        std::string errmsg("Call as <instance>:");
        errmsg += methodName;
        errmsg += "(...) (:, not .)";
        return luaL_error(_lua, errmsg.c_str());
    }

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));
    if(!object.valid()) {
        return luaL_error(_lua, "Parameter 1 is not a Lua2 instance");
    }

    Parameters inputParameters, outputParameters;

    // Marshal the input parameters
    for(int i = 2; i <= n; ++i) {
        inputParameters.insert(inputParameters.begin(), lse->popParameterObject());
    } //foreach input

    // Run it
    if(!_ci.run(object, methodName, inputParameters, outputParameters)) {
        return luaL_error(_lua, "Error while invoking method");
    }

    // Marshal the output parameters
    for(Parameters::iterator itr = outputParameters.begin();
        itr != outputParameters.end();
        ++itr)
    {
        lse->pushParameter(itr->get());
    }
    return outputParameters.size();
} //CF_MethodRunner

#if 0
/// Set or get a single-valued (non-vector, non-map) userdata value.
/// Syntax: lua2obj:ud('name') to get, or lua2obj:ud('name',value) to set.
/// Upvalues: 1 = LSE
///
/// NOTE: LuaCallbackObject instances are also stored in the userdata.  It may
/// be better NOT to provide access to OSG userdata from Lua --- after all,
/// Lua code can store arbitrary data in a Lua2 instance.  But then I would
/// ideally have a way for callbacks to retrieve the Lua2 instance from the
/// LSE parameter they are passed.
static int CF_UserDataAccessor(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));

    int n = lua_gettop(_lua);    /* number of arguments */
    if( (n<2) || (n>3) ) {
        return luaL_error(_lua,
            "Call as <instance>:ud('name') or <instance>:ud('name', <value>)");
    }

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));
    if(!object.valid()) {
        return luaL_error(_lua, "Parameter 1 is not a Lua2 instance");
    }

    osg::ref_ptr<osg::UserDataContainer> udc(object->getOrCreateUserDataContainer());
    if(!udc.valid()) {
        return luaL_error(_lua, "No user-data container available");
    }

    if(lua_type(_lua, 2) != LUA_TSTRING) {
        return luaL_error(_lua, "First parameter must be a string name");
    }

    const std::string name(lua_tostring(_lua, 2));

    if(n == 2) {    //Getter
        // Get a ValueObject
        const Object *udo = udc->getUserObject(name);
        if(!udo) {
            lua_pushnil(_lua);
        } else {
            // TODO
        }
        return 1;
    }

    // Setter
    // TODO Create a ValueObject
    udc->addUserObject(vo);
    return 0;
} //CF_UserDataAccessor
#endif //0

/// Make a Lua2 object instance from a LuaScriptEngine osg::Object instance
/// at the top of the lua stack.  "CF_" = Lua cfunction.
static int CF_makeLua2Object(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));
    int n = lua_gettop(_lua);    /* number of arguments */

    osg::ref_ptr<osg::Object> retval(0);

    if (n!=1) {
        return luaL_error(_lua, "Invalid call: syntax makeLua2Object(LSE_obj)");

    } else if (lua_type(_lua, 1)!=LUA_TTABLE) {
        return luaL_error(_lua, "Invalid call: syntax makeLua2Object(LSE_obj (a table))");

    } else {
        //get obj
        osg::ref_ptr<osg::Object> object = getObjectFromStack(lse, 1, true);
        if(!object.valid()) return luaL_error(_lua, "Parameter 1 is not an LSE object");

        lua_pop(_lua, 1);   // Remove the LSE instance              // ]

        pushLua2Object(lse, object);                                // Lua2Obj ]
    }

    return 1;   // The new object
} //CF_makeLua2Object

/// Return the actual address of an osg::Object, as a number.
/// \pre    The top of the Lua stack is a
///         Lua2 table representing an osg::Object.
/// \post   If the table was valid, the address is left at the stack top;
///         otherwise, nil is left at the stack top.
/// \return The number of result values pushed on the Lua stack, or a
///         luaL_error return.
static int CF_getObjectPtr(lua_State * _lua)
{
    // The LuaScriptEngine is available as an upvalue for consistency with
    // the other global functions created in initialize().  However, we don't
    // presently need to use lse.
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));

    if(!object.valid()) {
        lua_pushnil(_lua);
    } else {
        lua_pushnumber(_lua,    // push the numeric value
                static_cast<lua_Number>(
                    reinterpret_cast<unsigned long long>(object.get())
                )); // assume a pointer fits in an unsigned long long
    }
    return 1;
} //CF_getObjectPtr

/// Method to return a table listing the properties of a Lua2 object.
/// Upvalues: 1 = LSE
static int CF_DirLua2(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));

    int n = lua_gettop(_lua);    /* number of arguments */
    if (n!=1) return luaL_error(_lua, "syntax: object:dir()");

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));

    if(!object.valid()) {
        lua_pushnil(_lua);
        return 1;
    }

    lua_newtable(_lua);                             // T ]

    // Properties
    osgDB::ClassInterface::PropertyMap props;

    lua_pushstring(_lua, "properties");     // T1 K ]
    lua_newtable(_lua);                     // T1 K T2 ]
    if(_ci.getSupportedProperties(object, props)) {
        for(osgDB::ClassInterface::PropertyMap::const_iterator it = props.cbegin();
            it != props.cend();
            ++it
        ) {
            osgDB::BaseSerializer::Type ty(it->second);
            if( (ty != osgDB::BaseSerializer::RW_VECTOR) &&
                (ty != osgDB::BaseSerializer::RW_MAP) &&
                (ty != osgDB::BaseSerializer::RW_UNDEFINED) &&
                (ty != osgDB::BaseSerializer::RW_USER)
            ) { // TODO update this when I support vectors and maps
                lua_pushstring(_lua, it->first.c_str());    // T1 K T2 Propname ]
                lua_pushstring(_lua, _ci.getTypeName(it->second).c_str());  // T1 K T2 Propname Typestr ]
                lua_rawset(_lua, -3);                       // T1 K T2 ]
            }
        } //foreach property
    }
    lua_settable(_lua, -3);                 // T1 ]     stash properties

    // Methods
    osgDB::ClassInterface::PropertyMap methods;
    lua_pushstring(_lua, "methods");        // T1 K ]
    lua_newtable(_lua);                     // T1 K T3 ]
    if(_ci.getSupportedMethods(object, methods)) {
        int dest_idx = 1;   // index in the methods table
        for(osgDB::ClassInterface::PropertyMap::const_iterator it = methods.cbegin();
            it != methods.cend();
            ++it, ++dest_idx
        ) {
            lua_pushstring(_lua, it->first.c_str());    // T1 K T3 Methodname ]
            lua_rawseti(_lua, -2, dest_idx);            // T1 K T3 ]
        }
    }
    lua_settable(_lua, -3);                 // T1 ]     stash methods

#if 0
    // Userdata (in the OSG sense, not the Lua sense).
    // For now, only report the objects in the userdata container, not the
    // container's own userdata or the description list.
    const osg::UserDataContainer* udc = object->getUserDataContainer();
    lua_pushstring(_lua, "userdata");       // T1 K ]
    lua_newtable(_lua);                     // T1 K T4 ]

    if(udc) {
        int arr_part_idx = 0;   // for storing anonymous objects
        for(unsigned int i=0; i<udc->getNumUserObjects(); ++i) {
            // Get the object
            const osg::Object* uobj = udc->getUserObject(i);
            if(!uobj) continue;
            std::string uname = uobj->getName();

            // Push the key
            if(uname.empty()) {
                lua_pushnumber(_lua, ++arr_part_idx);   // T1 K T4 K4 ]
            } else {
                lua_pushstring(_lua, uname.c_str());    // T1 K T4 K4 ]
            }

            // Push the value
            lse->pushObject(const_cast<osg::Object *>(uobj));   // T1 K T4 K4 Obj ]

            // Stash it
            lua_rawset(_lua, -3);                       // T1 K T4 ]
        }
    }
    lua_settable(_lua, -3);                 // T1 ]
#endif

    return 1;
} //CF_DirLua2

/// Accessor for a Lua value that can be attached to any osg::Object.
/// Lua code calls `obj:lval()` to get `and obj:lval(val)` to set.  The value
/// is stored at a luaL_ref() index in the Lua registry, and the ref is stored
/// in an OSG UserObject.  This is the same way
/// lua::LuaScriptEngine::setPropertyFromStack() associates LUA_TFUNCTION
/// values with lua::LuaCallbackObject instances.
static int CF_LuaValueAccessor(lua_State *_lua)
{
    const lua::LuaScriptEngine* lse = reinterpret_cast<const lua::LuaScriptEngine*>(lua_topointer(_lua, lua_upvalueindex(1)));

    int n = lua_gettop(_lua);    /* number of arguments */
    if( (n<1) || (n>2) ) {
        return luaL_error(_lua, "Call as <instance>:lval([new value])");
    }

    osg::ref_ptr<osg::Object> object(getObjectFromStack(lse, 1));
    if(!object.valid()) {
        return luaL_error(_lua, "Parameter 1 is not a Lua2 instance");
    }

    (void)(object->getOrCreateUserDataContainer());     // make sure it has one

    if(n == 1) {    // Getter                           // Lua2 ]
        osg::ref_ptr<osg::Object> uo(
                osg::getUserObject(object, LUA2_TBL_USERVAL_NAME));
        IntValueObject* ivo = dynamic_cast<IntValueObject*>(uo.get());
        if (ivo)
        {
            lua_rawgeti(_lua, LUA_REGISTRYINDEX, ivo->getValue());  // Lua2 lval ]
            return 1;
        }

        // If there is no ivo, create a new table as the default.
        lua_newtable(_lua);                             // Lua2 newval ]
        // FALL THROUGH to the setter.
    }

    // Setter                                           // Lua2 newval ]
    {
        lua_pushvalue(_lua, -1);                            // Lua2 newval newval ]
        int ref = luaL_ref(_lua, LUA_REGISTRYINDEX);        // Lua2 newval ]
            // Saves newval to the registry
        osg::ref_ptr<IntValueObject> ivo(new IntValueObject(ref));
        ivo->setName(LUA2_TBL_USERVAL_NAME);    // udc doesn't assign names

        osg::UserDataContainer* udc = object->getOrCreateUserDataContainer();
        unsigned int objectIndex = udc->getUserObjectIndex(LUA2_TBL_USERVAL_NAME);
        if (objectIndex < udc->getNumUserObjects())
        {
            udc->setUserObject(objectIndex, ivo);
        }
        else
        {
            udc->addUserObject(ivo);
        }

        return 1;   // return a copy of newval
    }
} //CF_LuaValueAccessor

/// GC method to be called on a userdata.  Copied from LuaScriptEngine.cpp.
static int CF_garbageCollectObject(lua_State* _lua)
{
    int n = lua_gettop(_lua);    /* number of arguments */
    if (n==1)
    {
        if (lua_type(_lua, 1)==LUA_TUSERDATA)
        {
            osg::Object* object = *const_cast<osg::Object**>(reinterpret_cast<const osg::Object**>(lua_touserdata(_lua, 1)));
            object->unref();
        }
    }

    return 0;
} //CF_garbageCollectObject

void registerCFunctions(lua::LuaScriptEngine *lse)
{
    lua_State *_lua = lse->getLuaState();

    // provide global "cxx_bp" method that does nothing, but is a
    // place to put a C++ breakpoint.
    {
        lua_pushlightuserdata(_lua, lse);                   // ud ]
        lua_pushcclosure(_lua, cxx_bp, 1);                  // cc ]
        lua_setglobal(_lua, "cxx_bp");                      // ]
    }

    // Set up the __gc methods for looking up implementations of Object
    // pointer to do the unref when the associated Lua object is destroyed.
    // Each Lua2 object has an "osgObject" (LUA2_OBJ_PTR) userdata member that
    // calls Referenced::unref() on the corresponding osg::Object.
    // \see LuaScriptEngine::pushObject().
    {                                                       // ]
        luaL_newmetatable(_lua, LUA2_UNREF_MT);             // MT ]
        lua_pushstring(_lua, "__gc");                       // MT "" ]
        lua_pushlightuserdata(_lua, lse);                   // MT "" lse ]
        lua_pushcclosure(_lua, CF_garbageCollectObject, 1); // MT "" cc ]
        lua_settable(_lua, -3);                             // MT ]
        lua_pop(_lua,1);                                    // ]
    }

    // provide global "makeLua2Object" method for making a Lua2 object
    // from an LSE object.
    // testing identity of osg::Object instances.
    {
        lua_pushlightuserdata(_lua, lse);                   // ud ]
        lua_pushcclosure(_lua, CF_makeLua2Object, 1);       // cc ]
        lua_setglobal(_lua, "makeLua2Object");              // ]
    }

    // Set up the Lua2 root metatable.  Note: we do not use __newindex
    // so that regular Lua assignments actually appear on the table.
    lua_newtable(_lua);                             // MT ]

    // Set up this metatable to be used for property lookups
    lua_pushstring(_lua, "__index");                // MT K ]
    lua_pushvalue(_lua, -2);                        // MT K MT ]
    lua_settable(_lua, -3);                         // MT ]

    // add ptr() method to retrieve the actual address
    {
        lua_pushstring(_lua, "ptr");                // MT K ]
        lua_pushlightuserdata(_lua, lse);           // MT K lse ]
        lua_pushcclosure(_lua, CF_getObjectPtr, 1); // MT K cc ]
        lua_settable(_lua, -3);                     // MT ]
    }

    // add dir() method to retrieve the property names
    {
        lua_pushstring(_lua, "dir");                // MT K ]
        lua_pushlightuserdata(_lua, lse);           // MT K lse ]
        lua_pushcclosure(_lua, CF_DirLua2, 1);      // MT K cc ]
        lua_settable(_lua, -3);                     // MT ]
    }

    // add lval() method to access an associated Lua value
    {
        lua_pushstring(_lua, "lval");               // MT K ]
        lua_pushlightuserdata(_lua, lse);           // MT K lse ]
        lua_pushcclosure(_lua, CF_LuaValueAccessor, 1);     // MT K cc ]
        lua_settable(_lua, -3);                     // MT ]
    }

    lua_setglobal(_lua, LUA2_ROOT_METATABLE);       // ]

#if 0
    // TODO add accessor for OSG userdata
    {
        lua_pushstring(_lua, "ud");                 // MT K ]
        lua_pushlightuserdata(_lua, lse);           // MT K lse ]
        lua_pushcclosure(_lua, CF_UserDataAccessor, 1); // MT K cc ]
        lua_settable(_lua, -3);                     // MT ]
    }
#endif //0

    // Make the base the metatable for osg::Object
    pushMetatableForClass(lse, "osg::Object");      // MT ]
    lua_getglobal(_lua, LUA2_ROOT_METATABLE);       // MT RootMT ]
    lua_setmetatable(_lua, -2);                     // MT ]
    lua_pop(_lua, 1);                               // ]

} //registerCFunctions

// }}}1

/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2013 Robert Osfield
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/

// vi: set ts=4 sts=4 sw=4 et ai fo=crql: //
