Softbound 1 -------------------------------------------------------------------
----------------------------------------------------------------- July 29, 2017
---------------------------------------------  Avon Free Library, Avon, NY, USA

Softbound 1 was a microdemoparty and scene outreach for kids, organized by
cxw/Incline and run in association with the Avon Free Library.  We had about
15 kids, plus parents, which was at capacity for the partyplace.  The party
included:

 - Presentation: What is the demoscene?
 - Demoshow: Fair play to the queen, Koneko, Legomania
 - Livecoding
 - Introduction to Scratch
 - Open coding time - we had laptops for all the kids so they could play with
   Scratch.

A good time was had by all!

Thanks to Raven/NCE for a cool logo, SilentK for the promotional material and
presentation, and all the Avon Library and OWWL system folks for their support!

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

One release:

lua-osg-livecoding: Live-coding tool you can use to make 3D graphics using Lua.
                    By cxw/Incline.

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
