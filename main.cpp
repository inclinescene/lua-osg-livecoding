// main.cpp: Lua livecoding with OpenSceneGraph
// Uses code modified from osgviewer and from
// https://github.com/dpapavas/luaprompt.git .
// Original copyrights below.

// Copyright (c) 2017--2018 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

// Headers //////////////////////////////////////////////////////////// {{{1
#include "bulk.hpp"

#include <osg/GraphicsContext>
#include <osgViewer/config/SingleScreen>

#include "common.h"
#include <osg/Depth>

// For prompting
#include <sys/stat.h>
#include <signal.h>
#include <pthread.h>
    // *** POSIX threads, not Win32 ***
#include <setjmp.h>

#include <sys/ioctl.h>
#include <sys/select.h>

#include <glob.h>

#define LINENOISE_INTERNALS
#include "linenoise/linenoise.h"

#ifdef CONCRETE_LUA
#include "lua2.h"
#endif // CONCRETE_LUA

#include "audio_utsl.h"

using namespace osg;
using namespace std;
using namespace OpenThreads;

// }}}1
// Helper functions /////////////////////////////////////////////////// {{{1

#ifdef _DEBUG
/// Make a std::string.  For use at the gdb command line.
/// From https://stackoverflow.com/a/16735613/2877364
/// by https://stackoverflow.com/users/775806/n-m .
std::string& SSS (const char* s)
{
    return *(new std::string(s));
}
#endif // _DEBUG

// }}}1
// Prompting ////////////////////////////////////////////////////////// {{{1

// Helpers // {{{2
#define print_output(...) fprintf (stdout, __VA_ARGS__), fflush(stdout)
#define print_error(...) fprintf (stderr, __VA_ARGS__), fflush(stderr)

#define COLOR(i) (colorize ? colors[i] : "")

static string logfile(".livecoding.history");

static const int colorize = 1;
static const char *colors[] = {"\033[0m",
                               "\033[0;31m",
                               "\033[1;31m",
                               "\033[0;32m",
                               "\033[1;32m",
                               "\033[0;33m",
                               "\033[1;33m",
                               "\033[1m",
                               "\033[22m"};

#if 0
static void display_matches (char **matches, int num_matches, int max_length)
{
    print_output ("%s", COLOR(7));
    rl_display_match_list (matches, num_matches, max_length);
    print_output ("%s", COLOR(0));
    rl_on_new_line ();
}

static char *keyword_completions (const char *text, int state)
{
    static const char **c, *keywords[] = {
        "and", "break", "do", "else", "elseif", "end", "false", "for",
        "function", "if", "in", "local", "nil", "not", "or",
        "repeat", "return", "then", "true", "until", "while", NULL
    };

    int s, t;

    if (state == 0) {
        c = keywords - 1;
    }

    /* Loop through the list of keywords and return the ones that
     * match. */

    for (c += 1 ; *c ; c += 1) {
        s = strlen (*c);
        t = strlen(text);

        if (s >= t && !strncmp (*c, text, t)) {
            return strdup (*c);
        }
    }

    return NULL;
}


static char *generator (const char *text, int state)
{
    static int which;
    char *match = NULL;

    if (state == 0) {
        which = 0;
    }

    /* Try to complete a keyword. */

    if (which == 0) {
        if ((match = keyword_completions (text, state))) {
            return match;
        }
        which += 1;
        state = 0;
    }

    /* Try to complete a module name. */

    if (which == 1) {
#if 0
        if ((match = module_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a table access. */

    if (which == 2) {
#if 0
        look_up_metatable = 0;
        if ((match = table_key_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a metatable access. */

    if (which == 3) {
#if 0
        look_up_metatable = 1;
        if ((match = table_key_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a file name. */

    if (which == 4) {
        if (text[0] == '\'' || text[0] == '"') {
            match = rl_filename_completion_function (text + 1, state);

            if (match) {
                struct stat s;
                int n;

                n = strlen (match);
                stat(match, &s);

                /* If a match was produced, add the quote
                 * characters. */

                match = (char *)realloc (match, n + 3);
                memmove (match + 1, match, n);
                match[0] = text[0];

                /* If the file's a directory, add a trailing backslash
                 * and suppress the space, otherwise add the closing
                 * quote. */

                if (S_ISDIR(s.st_mode)) {
                    match[n + 1] = '/';

                    rl_completion_suppress_append = 1;
                } else {
                    match[n + 1] = text[0];
                }

                match[n + 2] = '\0';
            }
        }
    }

    return match;
}
#endif

static void finish ()
{
    /* Save the command history on exit. */

    if (!logfile.empty()) {
        linenoiseHistorySave(logfile.c_str());
    }
}

// }}}2
// CLI thread // {{{2

typedef WorkQueue<string> StringQ;
typedef WorkQueue< ref_ptr<ScriptError> > ResponseQ;

/// Thread that receives input from the user and sends it to the main thread
/// for execution.  Adapted from luaprompt.
class CLIThread: public Thread
{
public:
    /// Let Lua warnings through
    static bool show_warnings;

private:
    // Exit support
    int fd_request_exit_, fd_exit_requested_;

    // Prompt strings
    static const char *single;
    static const char *multi;

    /// Commands from CLIThread to the main thread
    StringQ& from_me_;

    /// Responses from the main thread to the CLIThread
    ResponseQ& to_me_;

    /// Prompts used, a la luaprompt.
    string prompts[2][2];

    void exec_string(const string& cmd, ref_ptr<ScriptError>& response)
    {
        // Suppress NOTICE messages so we don't get an error printout
        // on an incomplete line we are going to handle anyway.
        NotifySeverity severity = getNotifyLevel();
        if(!show_warnings && (severity >= NotifySeverity::NOTICE)) {
            // Higher numbers mean more messages --- if we are getting
            // NOTICE messages or more than that, drop the level to
            // WARN until the command is done.
            setNotifyLevel(NotifySeverity::WARN);
        }

        from_me_.put(cmd);    // Execute
        to_me_.get(response);       // Block for a response

        // Restore the severity level
        setNotifyLevel(severity);
    } //exec_string

    enum Resp { OK, Error, Incomplete };

    /// The last error message we got.
    string lastErr_;

    Resp handle_response(ref_ptr<ScriptError>& response)
    {
        if(response.get() == 0) {
            OSG_NOTICE << "NULL ScriptError object" << endl;
            return OK;      // Discard any partial comment

        } else if(response->getErrorMessage().find("<eof>") != string::npos) {
            //partial command - leave cmd untouched.
            return Incomplete;

        } else if(response->getErrorMessage() == "no error") {
            return OK;

        } else {    // Error.  It's up to the caller to report it if desired.
            //cout << "Response to -" << cmd << "-: " << endl;
            //cout << response->getErrorMessage() << endl;
            lastErr_ = response->getErrorMessage();
            //cout << "FN  " << response->getFileName() << endl;
            //cout << "Ln  " << response->getLineNumber() << endl;
            //cout << "TB  " << response->getTracebackMessage() << endl;
            cout << endl;

            return Error;
        }
    } //handle_response

    // --- Linenoise customization ---

    static CLIThread *me_;  ///< The CLIThread instance currently running cliReadCode

    /// Read bytes of the next character based on *me_.
    static size_t cliReadCode(int fd, char *buf, size_t buf_len, int *cp) {
        if(!buf || !cp || buf_len<1) {
            errno = EINVAL;
            return -1;
        }

        while(1) {

            // Wait for data on the exit-request pipe or the terminal
            int maxfd = me_->fd_exit_requested_;
            if(fd > maxfd) maxfd = fd;

            fd_set rfds;
            FD_ZERO(&rfds);
            FD_SET(fd, &rfds);
            FD_SET(me_->fd_exit_requested_, &rfds);

            int selret = select(maxfd+1, &rfds, NULL, NULL, NULL);
            if(selret == -1) {
                int olderrno = errno;
                //OSG_WARN << "Select error" << endl;
                errno = olderrno;   // in case cout resets errno
                return -1;
            } else if(!selret) {
                //OSG_WARN << "No data" << endl;
                continue;   // keep waiting
            }

            // If we get here, there is data.
            if(FD_ISSET(me_->fd_exit_requested_, &rfds)) {
                char exitbuf[4] = {0};
                int nread = read(me_->fd_exit_requested_, exitbuf, 1);
                if(nread == -1) {
                    int olderrno = errno;
                    //OSG_WARN << "pipefd read error " << errno << endl;
                    errno = olderrno;
                    return -1;
                }
                //cout << "Exiting - got " << buf << endl;
                errno = 0;  // no error
                return 0;   // EOF
            } //endif exiting

            if(FD_ISSET(fd, &rfds)) {
                int nread = read(fd, buf, 1);
                if(nread == -1) {
                    int olderrno = errno;
                    //OSG_WARN << "stdin read error " << errno << endl;
                    errno = olderrno;
                    return -1;
                }
                //fprintf(stderr, "Got %02x\n", (int)buf[0]);

                if(nread == 1) *cp = buf[0];
                return nread;
            } //endif stdin bytes

        } //while

    } //cliReadCode()

public:
    /// Set by other threads to indicate this thread should exit
    bool should_exit;

    /// Set by this thread to indicate it is already exiting in response
    /// to a user "exit" command
    bool is_exiting;

    void requestExit()
    {
        char c = 'q';   // anything nonzero
        write(fd_request_exit_, (const void *)&c, 1);
    } //requestExit()

    CLIThread(StringQ& cmds, ResponseQ& responses)
    : Thread()
    , from_me_(cmds)
    , to_me_(responses)
    , should_exit(false)
    , is_exiting(false)
    {
        OSG_NOTICE << "CLIThread created" << endl;

        // Exit pipes
        int fds[2];
        if(pipe(fds) != 0) {
            throw runtime_error("Could not create pipe");
        }
        fd_request_exit_ = fds[1];      //write
        fd_exit_requested_ = fds[0];    //read

        /* Plain, uncolored prompts. */

        prompts[0][0] = single;
        prompts[0][1] = multi;

        /* Colored prompts. */

        char buf[256];  // Fixed buffer size = bad!
        sprintf (buf, "%s%s%s",
                 COLOR(6), single, COLOR(0));
        prompts[1][0] = buf;
        sprintf (buf, "%s%s%s",
                 COLOR(6), multi, COLOR(0));
        prompts[1][1] = buf;

#if 0
        rl_readline_name = "livecoding";
        rl_basic_word_break_characters = " \t\n`@$><=;|&{(";
        rl_completion_entry_function = generator;
        rl_completion_display_matches_hook = display_matches;

#endif
        /* Load the command history if there is one. */
        if (!logfile.empty()) {
            linenoiseHistoryLoad(logfile.c_str());
        }

        linenoiseSetMultiLine(true);
        if(enableRawMode(STDIN_FILENO) == -1) {
            throw runtime_error("Couldn't enable raw mode");
        }

        me_ = this;
        linenoiseSetEncodingFunctions(
            defaultPrevCharLen,
            defaultNextCharLen,
            cliReadCode
        );

    } //ctor

    virtual ~CLIThread() {
        OSG_NOTICE << "CLIThread destructor called in thread " <<
            hex << pthread_self() << endl;
        disableRawMode(STDIN_FILENO);
        // Save the updated command history
        if (!logfile.empty()) {
            linenoiseHistorySave(logfile.c_str());
        }
    }

    virtual void run()   //luap_enter(lua_State *L)
    {
        OSG_NOTICE << "CLIThread run() called in thread " <<
            hex << pthread_self() << endl;

        try {   // Note: even this doesn't catch SEGV in cli_innards()
            cli_innards();
            cout << "after cli_innards()" << endl;
            fflush(stdout);
        } catch(exception& e) {
            OSG_WARN << "Uncaught exception in cli_innards(): " << e.what() << endl;
            fflush(stderr);
        } catch(...) {
            OSG_WARN << "There was an uncaught custom exception in cli_innards().\n";
            fflush(stderr);
        }
    }

    virtual void cli_innards()
    {
        cout << endl << "*** lua-osg-livecoding CLI ***" << endl;
        cout << "by cxw/Incline 2017--2018" << endl << endl;
        cout << "Built-in commands are exit, camon, camoff, help()." << endl;


        string cmd;                 ///< the string from the user
        bool incomplete(false);     ///< whether we're in the middle of a command

        ref_ptr<ScriptError> response;

        while (!should_exit) {

            // Get a line
            char *line = linenoise_R(incomplete ?
                           prompts[colorize][1].c_str() :
                           prompts[colorize][0].c_str());
            int theerrno = errno;

            if(!line && (theerrno == EAGAIN)) {    // Ctrl-C
                incomplete = false;     // Discard any command in progress
                cmd.clear();
                continue;

            } else if(!line) {                  // Ctrl-D
                break;
            }

            if (*line == '\0') {    //empty line?
                free(line);
                continue;
            }

            // Add/copy the line to the buffer.

            if (incomplete) {
                cmd += ' ';
                cmd += line;
            } else {    // !incomplete
                cmd = line;
            }
            free(line);

            Resp r;
            string to_exec;

            if(cmd=="exit") {
                from_me_.put(cmd);
                break;
            }

            // Special commands
            if(cmd=="camon" || cmd=="camoff") {
                to_exec = cmd;

            } else if(incomplete) {         // Don't try to execute incomplete
                r = Incomplete;             // statements as expressions.
                to_exec = cmd;

            } else {
                // Try to execute fresh lines as an expression to be printed.

                // TODO? use a "return" instead of a "print" and get the
                // output parameter from the script?
                to_exec = "print_r( (";
                    // We already loaded print_r in init()
                to_exec += cmd;
                to_exec += ") );";  //Try to let the interpreter know this is it
            }

            exec_string(to_exec, response);
            r = handle_response(response);

            if(r == OK) {
                incomplete = false;

            } else if( (!incomplete) && (r == Incomplete) ) {
                // It wasn't incomplete, so we tried to execute it as an
                // expression, and we got an Incomplete.  This is odd.
                OSG_NOTICE << "Incomplete in an expression?!!??"  << endl
                    << " => `" << cmd << '`' << endl;

            } else if( incomplete && (r == Incomplete) ) {
                (void)0;    // It was incomplete, and it still is - do nothing.

            } else {    // A syntax error.  Maybe it's a statement?
                /* Try to execute the line as-is. */

                exec_string(cmd, response);
                r = handle_response(response);

                incomplete = false;

                if(r == Incomplete) {
                    incomplete = true;
                } else if(r == Error) {
                    cout << lastErr_ << endl;
                    lastErr_ = "";
                }
            }

            /* Add the line to the history if non-empty. */

            if (!incomplete) {
                linenoiseHistoryAdd(cmd.c_str());
            }
        } //while(!should_exit)

        is_exiting = true;
        from_me_.put("exit");   // Tell the main thread to exit
        print_output ("\n");
        OSG_NOTICE << "CLIThread run() exiting from thread " << pthread_self()
                    << endl;
    } //cli_innards

}; //CLIThread

const char *CLIThread::single = "Cmd? (or 'exit') > ";
const char *CLIThread::multi =  "                 > ";
bool CLIThread::show_warnings = false;
CLIThread *CLIThread::me_ = 0;

// }}}2
// }}}1
// Lua runner ///////////////////////////////////////////////////////// {{{1

/// Name of a Lua function that returns a command we should run
const string GET_CMD_FROM_LUA("cxx_get_cmd");

/// RunLuaHandler telling main() to turn on or off the camera manipulator
static volatile enum { NoCamCmd, TurnOn, TurnOff } CamManipCmd = NoCamCmd;
    // TODO figure out a way to permit Lua to control the manipulator

/// Run a Lua command, if one has been sent over the pipe.  This is an event
/// callback and not an update callback because we need access to the event
/// queue in order to trigger an exit.
class RunLuaHandler: public osgGA::GUIEventHandler {
private:

    StringQ& to_me_;
    ResponseQ& from_me_;

    /// A response we will reuse for all successful commands
    ref_ptr<ScriptError> no_error_;

    /// Contents of the command from Lua
    string specialCommand_;

public:
    /// A script with useful C++ interface routines.  Public so the
    /// main loop can access it.
    ref_ptr<Script> cpp_interface;
    ref_ptr<ScriptEngine> engine;

    RunLuaHandler(ScriptEngine *theengine, StringQ& cmds, ResponseQ& responses)
    : to_me_(cmds)
    , from_me_(responses)
    , no_error_(new ScriptError("no error"))
    , cpp_interface(0)
    , engine(theengine)
    {
        init();
    }

    /// Return and reset the special command
    string specialCommand() {
        string retval = specialCommand_;
        specialCommand_ = "";
        return retval;
    }

    /// Load Lua functions we always want available
    void init()
    {
        Parameters inparms, outparms;

        // Load print_r
        inparms.clear(); outparms.clear();
        cout << "Ignore the warning about \"getType\" - it's from the "
            "`return print_r` at the end of print_r.lua." << endl;
        if(!execScriptFile("lua/print_r.lua", engine, inparms, outparms)) {
            OSG_NOTICE << "Could not load print_r" << endl;
        }

        // Load cpp_interface
        cpp_interface = osgDB::readRefScriptFile("lua/cpp-interface.lua");
        inparms.clear(); outparms.clear();
        if(!engine->run(cpp_interface, "", inparms, outparms)) {
            throw runtime_error("Could not load cpp_interface");
        }
    } //init

    /// Every frame, run a pending Lua command from the user, if there
    /// is one to run.  Also run things the Lua code wants us to run.
    virtual bool handle(const osgGA::GUIEventAdapter& ea,
                        osgGA::GUIActionAdapter& aa,
                        osg::Object*,
                        osg::NodeVisitor* nv)
    {
        if(ea.getEventType() != osgGA::GUIEventAdapter::FRAME) {
            return false;
        }

        //---------------------
        // Once per frame, run Lua code.

        { // Call the Lua perFrameEvent() function
            auto fs = nv->getFrameStamp();
            auto sim_time = fs->getSimulationTime();

            Parameters inparms, outparms;
            inparms.push_back(new DoubleValueObject(sim_time));

            engine->run(cpp_interface, "perFrameEvent", inparms, outparms);
                // Ignore return value - one error message per frame
                // would be ugly.
        }

        //---------------------
        // Do something the Lua code wants us to do (if anything)
        do {
            Parameters inparms, outparms, outp2;

            // Read the command
            bool ok = engine->run(cpp_interface, GET_CMD_FROM_LUA,
                    inparms, outparms);

            // Process the command, if we got one
            if(!ok) {
                break;  // Ignore error message for now
            }

            if(outparms.size()!=1) break;

            StringValueObject *svo =
                dynamic_cast<StringValueObject*>(outparms[0].get());
            if(!svo) break;

            const string cmdFromLua(svo->getValue());
            if(!cmdFromLua.empty()) {
                cout << "Got command from Lua: " << cmdFromLua << endl;
                specialCommand_ = cmdFromLua;
            }

        } while(0);

        //---------------------
        // Run a Lua command from the user (if any)
        string cmd;

        if(!to_me_.getIfAvailable(cmd)) {
            return false;
        }

        // Special-purpose commands
        if(cmd=="exit") {
            auto view = aa.asView();
            osgViewer::View* vview = dynamic_cast<osgViewer::View*>(view);
            if(!vview) {
                OSG_NOTICE << "Could not get osgViewer::View" << endl;
            } else {
                ref_ptr<osgGA::GUIEventAdapter> event(
                                                new osgGA::GUIEventAdapter);
                event->setEventType(osgGA::GUIEventAdapter::QUIT_APPLICATION);

                auto queue = vview->getEventQueue();
                queue->addEvent(event);
                    // EventQueue holds ref_ptr's, so it's OK that our
                    // ref_ptr goes out of scope here.
            }
            from_me_.put(no_error_);    // let the CLI thread keep going

        } else if(cmd=="camon" && CamManipCmd == NoCamCmd) {
            cout << "Requesting manipulator on" << endl;
            CamManipCmd = TurnOn;
            from_me_.put(no_error_);

        } else if(cmd=="camoff" && CamManipCmd == NoCamCmd) {
            cout << "Requesting manipulator off" << endl;
            CamManipCmd = TurnOff;
            from_me_.put(no_error_);

        } else {    // General commands
            ref_ptr<Script> script(new Script("lua",cmd));
            Parameters inparms, outparms;

            if(!engine->run(script, "", inparms, outparms)) {
                //cout << "Could not run command -" << cmd << "-" << endl;
                from_me_.put(engine->getLastSavedError());
            } else {
                from_me_.put(no_error_);
            }
        }

        return false;   // false => other handlers get the FRAME event, too.
    } //handle()

}; //RunLuaHandler

bool setLuaGlobal(RunLuaHandler *lua, const std::string& name, Object *val)
{
    if(!lua->cpp_interface) {
        return false;
    } else {
        Parameters inparms, outparms;
        inparms.clear(); outparms.clear();
        inparms.push_back(new StringValueObject(name));
        inparms.push_back(val);
        if(!lua->engine->run(lua->cpp_interface, "loadGlobal", inparms, outparms))
        {
            OSG_WARN << "Could not set global " << name << endl;
            return false;
        }
    }
    return true;
}

bool loadLuaArgv(RunLuaHandler *lua, const deque<string>& args)
{
    if(args.empty()) return true;    // nothing to do

    if(!lua->cpp_interface) {
        return false;
    } else {
        Parameters inparms, outparms;
        inparms.clear(); outparms.clear();
        for(deque<string>::const_iterator it = args.begin(); it!=args.end(); ++it) {
            inparms.push_back(new StringValueObject(*it));
        }

        if(!lua->engine->run(lua->cpp_interface, "loadArgv", inparms, outparms))
        {
            OSG_WARN << "Could not load argv" << endl;
            return false;
        }
    }
    return true;
}

// }}}1
// HUD and background ///////////////////////////////////////////////// {{{1

typedef Incline::Content<Vec3Array, Vec4Array, Vec2Array> HUDContent;

/// Create a camera to draw orthographically in the window,
/// and a subgraph to draw in that camera.  Modified from osghud.
/// @param viewer {osg::Viewer}
/// @param order {osg::Camera::RenderOrder}
/// @param startBlack {bool} If true, and if #addContent, make the initial
///         quad black rather than colorful
/// @param addContent {bool} If true, add an initial quad.  Otherwise, the
///         new camera will have no children.
ref_ptr<Camera> createHUD(ref_ptr<osgViewer::Viewer> viewer,
                            const Camera::RenderOrder order,
                            const bool startBlack = false,
                            const bool addContent = true)
{
    ref_ptr<Camera> camera(new osg::Camera);

    osgViewer::Viewer::Windows windows;
    int w, h;
    viewer->getWindows(windows);

    if (windows.empty()) return 0;

    w = windows[0]->getTraits()->width;
    h = windows[0]->getTraits()->height;

    // set up cameras to render on the first window available.
    camera->setGraphicsContext(windows[0]);
        // This causes the graphics context to run the camera's rendering
        // in the right order (PRE or POST) (see
        // osg::GraphicsContext::runOperations)  However, you still have to
        // add the camera as a slave or a node in order for it to show up.
        //
        // Without this call, bg_as_node cameras still work, but slave
        // cameras do not.
    camera->setViewport(0,0, w, h);

    // set the projection matrix
    camera->setProjectionMatrix(osg::Matrix::ortho2D(0,1,0,1));
        // Window size is in the viewport.
    camera->setProjectionResizePolicy(Camera::ProjectionResizePolicy::FIXED);
        // Object coordinates will always be (0,0)->(1,1) regardless of
        // window resizes

    // set the view matrix
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera->setViewMatrix(osg::Matrix::identity()); //Pixel coords

    // Never cull
    camera->setCullingActive(false);

    // clear depending on which order it is
    if(order==Camera::PRE_RENDER) {
        camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        camera->setClearColor(
                startBlack ? osg::Vec4(0,0,0,1) : osg::Vec4(0.1f,0.3f,0.3f,1.0f)
        );
    } else {
        camera->setClearMask(GL_DEPTH_BUFFER_BIT);
    }

    camera->setRenderOrder(order);
    camera->setRenderTargetImplementation(Camera::FRAME_BUFFER);
    auto css = camera->getOrCreateStateSet();
    css->setMode(GL_LIGHTING, StateAttribute::OFF);

    if(order==Camera::PRE_RENDER) {
        css->setAttributeAndModes(
          new osg::Depth(osg::Depth::Function::ALWAYS, 1.0, 1.0, false), StateAttribute::ON);
        css->setMode(GL_DEPTH_TEST, StateAttribute::OFF);
    } else {    //post render
        css->setAttributeAndModes(
          new osg::Depth(osg::Depth::Function::LEQUAL, 1.0, 1.0), StateAttribute::ON);
        css->setMode(GL_DEPTH_TEST, StateAttribute::ON);
    }

    // we don't want the camera to grab event focus from the viewers main camera(s).
    camera->setAllowEventFocus(false);

    /////////////// Make geometry

    if(addContent) {
        ref_ptr<HUDContent> content(new HUDContent({"pos","color", "texture"}));
        const float c(startBlack ? 0.0 : 1.0);

        // Make a quad
        content->startbuild(PrimitiveSet::TRIANGLE_STRIP);
        content->newvload(0,0,0, 0,0,0,1, 0,0);
        content->newvload(1,0,0, c,0,0,1, 1,0);
        content->newvload(0,1,0, 0,c,0,1, 0,1);
        content->newvload(1,1,0, c,c,0,1, 1,1);
        content->endbuild();
        // Hack it so we don't need a shader for now
        content->setVertexArray(content->getArray(0));
        content->setColorArray(content->getArray(1));
        content->setTexCoordArray(0,content->getArray(2));

        camera->addChild(content);
    }

    return camera;
} //createHUD

/// Set up a camera for a background behind all the rendered geometry.
/// Modified from osghud, osgprerender.
ref_ptr<Camera> setupBGFG(ref_ptr<osgViewer::Viewer> viewer,
                                const bool pre_camera, const bool bg_as_node,
                                const bool startBlack = false,
                                const bool addContent = true,
                                const bool onTop = false)
{
    ref_ptr<Camera> hudCamera;

    if(pre_camera) {        //PRE_RENDER camera
        OSG_NOTICE << "Pre-render camera" << std::endl;
        hudCamera = createHUD(viewer, Camera::PRE_RENDER, startBlack, addContent);
        if(hudCamera)  {
            viewer->getCamera()->setClearMask(GL_DEPTH_BUFFER_BIT);
                // Don't clear the background, since hudCamera already has
            if(onTop) {
                OSG_WARN << "setupBGFG: onTop ignored for "
                    "pre-render camera" << endl;
            }
        }
    } else {                //POST_RENDER camera
        OSG_NOTICE << "Post-render camera" << std::endl;
        hudCamera = createHUD(viewer, Camera::POST_RENDER, startBlack, addContent);
        if(hudCamera)  {
            viewer->getCamera()->setClearMask(
                            GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
                // Normal

            if(!onTop) {
                hudCamera->setClearMask(0);
                // Don't clear - the framebuffer already has content
            }
        }
    }

    if(hudCamera && bg_as_node) {
        hudCamera->setName("hud as node");
        ref_ptr<Group> scene(dynamic_cast<Group*>(viewer->getSceneData()));
        if(!scene) {
            OSG_NOTICE << "Can't create bg camera as node if scene data isn't a group" << std::endl;
            return 0;
        }
        scene->addChild(hudCamera);

    } else if(hudCamera) {  // !bg_as_node => slave camera
        hudCamera->setName("hud as slave");
        viewer->addSlave(hudCamera, false);
            // false because it has its own scene
    } //else there is no camera to add.

    return hudCamera;

} //setupBGFG

// }}}1
// Uniforms /////////////////////////////////////////////////////////// {{{1

ref_ptr<Uniform> setupResolutionUniform(ref_ptr<osgViewer::Viewer> viewer)
{
    ref_ptr<Uniform> iResolution(
            new osg::Uniform(osg::Uniform::FLOAT_VEC2, "iResolution"));

    // Set the initial value, since the resize handler doesn't fire on startup.
    // Per http://forum.openscenegraph.org/viewtopic.php?p=41978#41978 ,
    // Viewport may be NULL if multiple windows are in use.
    do {
        // Try the master camera
        ref_ptr<const Viewport> vp(viewer->getCamera()->getViewport());
        if(vp) {
            Vec2f v(vp->width(), vp->height());
            iResolution->set(v);
            cout << "Running on master camera " << v.x() << 'x' << v.y() << endl;
            break;
        }

        // Try the slave cameras.  osg/src/osgViewer/config/AcrossAllScreens
        // appears to assume that all the windows are laid out left-to-right
        // for non-stereo, so do the same.  TODO?  Support stereo?
        osgViewer::ViewerBase::Cameras cameras;
        viewer->getCameras(cameras);
        float totalwid=0.0, maxht=0.0;

        for(size_t camidx=0; camidx < cameras.size(); ++camidx) {
            const Camera *cam = cameras[camidx];
            if(!cam) break;
            vp = cam->getViewport();
            if(!vp) break;

            cout << "Camera " << camidx << ": ";
            cout << "  " << vp->width() << 'x' << vp->height()
                << "@(" << vp->x() << ',' << vp->y() << ')' << endl;

            totalwid += vp->width();
            if(vp->height() > maxht) { maxht = vp->height(); }
            // Check which screen it's on
            const GraphicsContext *gc = cam->getGraphicsContext();
            if(!gc) continue;
            const GraphicsContext::Traits *traits = gc->getTraits();
                // Lives as long as the gc does
            if(!traits) continue;
            cout << "  Traits: "
                << traits->width << 'x' << traits->height
                << "@(" << traits->x << ',' << traits->y << ')' << endl;
            cout << "  On host " <<
                (traits->hostName.empty() ? "<anonymous>" :
                 traits->hostName) << endl;
            cout << "  Display " << traits->displayNum
                << "; screen " << traits->screenNum << endl;
        }
        if(cameras.size()>0) {
            Vec2f v(totalwid, maxht);
            iResolution->set(v);
            cout << "Running on " << cameras.size()
                << " slave camera(s): " << v.x() << 'x' << v.y() << endl;
            break;
        }

        throw runtime_error(
            "Can't find a viewport --- set OSG_WINDOW before running, or specify -c <config filename>"
        );
    } while(0);

    // Add the uniforms before adding the program, since that's what osgshaders does
    viewer->getSceneData()->getOrCreateStateSet()->addUniform(iResolution);
    viewer->addEventHandler(new ResolutionResizeHandler(iResolution));
        // TODO update ResolutionResizeHandler to take multiple screens
        // into account.

    return iResolution;
} //setupResolutionUniform

class UpdateTimeCallback : public Callback {
private:
    ref_ptr<Uniform> u_;
public:
    UpdateTimeCallback(Uniform *u): u_(u) {}
    virtual bool run(Object* object, Object* data)
    {
        do {
            auto nv = data->asNodeVisitor();
            if(!nv) break;
            auto fs = nv->getFrameStamp();
            if(!fs) break;
            u_->set((float)fs->getSimulationTime());
        } while(0);
        return Callback::run(object, data);
    } //run
}; //UpdateTimeCallback

// }}}1
// Version checking /////////////////////////////////////////////////// {{{1
// From https://vicrucann.github.io/tutorials/osg-version-opengl/

class TestSupportOperation : public osg::GraphicsOperation
{
    //typedef const GLubyte*  (*GLGETSTRING)(GLenum);

public:
    TestSupportOperation()
        : osg::Referenced(true)
        , osg::GraphicsOperation("TestSupportOperation", false)
        , m_supported(true)
        , m_errorMsg()
        , m_version(0.0)
    {}

    virtual void operator() (osg::GraphicsContext* gc)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);
        osg::GLExtensions* gl2ext = gc->getState()->get<osg::GLExtensions>();

        if( gl2ext ){

            if( !gl2ext->isGlslSupported )
            {
                m_supported = false;
                m_errorMsg = "ERROR: GLSL not supported by OpenGL driver.";
            }
            else
            {
                m_version = gl2ext->glVersion;
                glslversion = gl2ext->glslLanguageVersion;

                // Ideas from http://forum.openscenegraph.org/viewtopic.php?t=7004
                // and https://stackoverflow.com/a/126553/2877364 by
                // https://stackoverflow.com/users/6799/nearaz
                //GLGETSTRING gl_get_string = (GLGETSTRING)getGLExtensionFuncPtr("glGetString");
                //if(gl_get_string) {
                //gc->makeCurrent();
                // For some reason these return NULL, even with the makeCurrent call first.  TODO figure out later.
                //    vendor = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
                //    renderer = reinterpret_cast<const char*>(glGetString(GL_RENDERER));
                //    extensions = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
                //    version = reinterpret_cast<const char*>(glGetString(GL_VERSION));
                //}
            }
        } else {
            m_supported = false;
            m_errorMsg = "ERROR: GLSL not supported.";
        }
    }

    OpenThreads::Mutex  m_mutex;
    bool                m_supported;
    std::string         m_errorMsg;
    float               m_version;
    float               glslversion;
    //std::string         vendor, renderer, extensions, version;
}; //class TestSupportOperation

void printOpenGLVersionAndRealize(osgViewer::Viewer* viewer)
{
    osg::ref_ptr<TestSupportOperation> tester = new TestSupportOperation;
    viewer->setRealizeOperation(tester.get());
    viewer->realize();

    if (tester->m_supported) {
        std::cout << "=======================================" << endl;
        std::cout << "GLVersion=   " << tester->m_version << std::endl;
        std::cout << "GLSL Version=" << tester->glslversion << std::endl;
        //std::cout << "Vendor=      " << tester->vendor << std::endl;
        //std::cout << "Renderer=    " << tester->renderer << std::endl;
        //std::cout << "Extensions=  " << tester->extensions << std::endl;
        //std::cout << "Version=     " << tester->version << std::endl;
        std::cout << "=======================================" << endl;
    } else {
        std::cout << tester->m_errorMsg << std::endl;
    }
}

// }}}1
// Command-line and environment handling ////////////////////////////// {{{1

string optRun("");              ///< Lua file to run on startup (LOL_RUN)
bool optCamOn(false);           ///< Whether to leave the camera manipulator on
HAU soundStream(0);             ///< audio-utsl output stream for the music
string soundtrackFilename("");  ///< Music to play (if any)
//TODO copy length out to the caller

/// Whether to start with a black background and no axis markers
bool startBlack(false);

bool exitOnMusicStop(false);    ///< Whether to exit when the music ends
string viewerConfigFilename("");    ///< *.view filename to use, if any

/// Single screen to set up on.  Overridden by viewerConfigFilename.
int setupOnScreenNum(-1);

deque<string> remainingArgs;    ///< What's left --- goes to Lua

/// Load music from file #filename.  Returns 0 on success; nonzero on error.
int loadSoundtrack(const string& filename)
{
    if(!Au_Startup()) {     // idempotent, so OK to call multiple times.
        OSG_WARN << "Couldn't initialize audio" << endl;
        return 1;
    }

    int samplerate, channels;
    Au_SampleFormat format;
    long int len = -1;

    if(!Au_InspectFile(filename.c_str(), &samplerate, &channels, &format,
                            &len)) {
        OSG_WARN << "Couldn't decipher file " << filename << endl;
        return 1;
    }

    if(!(soundStream=Au_New(format, samplerate, channels, NULL))) {
        fprintf(stderr, "Couldn't create audio stream to play %s "
                "(%d ch, %d Hz, format=%d)\n", filename.c_str(), channels,
                samplerate, format);
        return 1;
    }

    soundtrackFilename = filename;
    return 0;
} //loadSoundtrack()

int ParseArguments(int argc, char **argv)
{ // Much drawn from osgviewer

    deque<string> real_args;
    int first_arg_idx = 1;

    // Read an atfile, if any -----------------------------------------
    real_args.push_back(argv[0]);

    if(argc>1 && argv[1][0] == '@') {
        first_arg_idx = 2;          // below, skip the @<filename>
        ifstream fd(&argv[1][1]);
        string line;
        while(fd >> line) {
            real_args.push_back(line);
        }
    }

    // Get any remaining command-line arguments
    bool cxx_args(true);
    for(int argidx = first_arg_idx; argidx<argc; ++argidx) {
        if(!strcmp(argv[argidx], "--")) {       // check for end-of-args marker
            cxx_args = false;
            continue;
        }
        if(cxx_args) {
            real_args.push_back(argv[argidx]);
        } else {
            remainingArgs.push_back(argv[argidx]);
        }
    }

    // Convert back to argc/argv form for ArgumentParser
    int inner_argc = real_args.size();
    char **inner_argv = new char*[inner_argc+1];
    char null_str[1] = {0};
    inner_argv[inner_argc] = null_str;
    vector<char *> to_free;     // Save the arg pointers since ArgumentParser
                                // rearranges things.

    for(int argidx=0; argidx < inner_argc; ++argidx) {
        int nchars = real_args[argidx].size();
        inner_argv[argidx] = new char[nchars+1];
        to_free.push_back(inner_argv[argidx]);

        strncpy(inner_argv[argidx], real_args[argidx].c_str(), nchars);
        inner_argv[argidx][nchars] = '\0';
    }

    // Populate basic data --------------------------------------------

    ArgumentParser arguments(&inner_argc, inner_argv);
        // Each call to arguments.read() will remove corresponding element(s)
        // from argv if appropriate, and will update argc.
        // You can add environment variables to the help, but ArgumentParser
        // won't help you check them --- you still have to manually call getenv.

    arguments.getApplicationUsage()->setApplicationName(arguments.getApplicationName());
    arguments.getApplicationUsage()->setDescription(
            arguments.getApplicationName()+
            " is a Lua livecoding environment and demo player by cxw/Incline.");

    // Populate arguments ---------------------------------------------

    arguments.getApplicationUsage()->addCommandLineOption(
            "-c <filename>", "Filename of viewer configuration (e.g., "
                            "AllScreens.view).  Overrides --screen.");

    arguments.getApplicationUsage()->addCommandLineOption(
            "-r <filename>", "Run the Lua script <filename> (e.g., a demo)");
        // initial script to run.  The script is actually run from livecoding.lua,
        // but we check it here for camera-manipulator control.

    arguments.getApplicationUsage()->addCommandLineOption(
            "-o", "Leave the camera manipulator on after running a -r script");
        // If set, always enable the camera manipulator at startup.

    arguments.getApplicationUsage()->addCommandLineOption(
            "-m <filename>", "Filename of music to play (e.g., WAV or OGG)");
        // If music provided, use it as the timebase.

    arguments.getApplicationUsage()->addCommandLineOption(
            "--music-exit", "Exit when the music finishes");

    arguments.getApplicationUsage()->addCommandLineOption(
            "--no-music-exit", "Do not exit when the music finishes");

    arguments.getApplicationUsage()->addCommandLineOption(
            "-v", "Print extra output.  Specify more times for more output.");

    arguments.getApplicationUsage()->addCommandLineOption(
            "--black", "Start with an all-black screen: black background and no "
                        "axis markers.");

    arguments.getApplicationUsage()->addCommandLineOption(
            "--screen <num>", "Run fullscreen on screen <num> (0, 1, ...).  "
                                "Overriden by -c.");

    arguments.getApplicationUsage()->addCommandLineOption(
            "--demo <lua> <music>", "Run <lua> as a demo with soundtrack "
                                    "<music>.  Implies -r -m --black "
                                    "--music-exit --screen 0 -o-.");

    // Check the arguments we handle here -----------------------------
    unsigned int helpType = 0;
    if ((helpType = arguments.readHelpType()))
    {
        arguments.getApplicationUsage()->write(cout, helpType);
        return 1;
    }

    // Read the arguments ---------------------------------------------

    string soundtrack_filename(soundtrackFilename);

    while(arguments.containsOptions()) {
        // Loop over the options so later options take precedence over
        // earlier options.  If a single option is given multiple times,
        // only the last one takes effect.
        // arguments.read(1, ...) means to only check the first argument
        // each time through the loop.

        if(arguments.read(1, "-c", viewerConfigFilename)) {}
        if(arguments.read(1, "-r", optRun)) {}

        if(arguments.read(1, "-o")) { optCamOn = true; }
        if(arguments.read(1, "--black")) { startBlack = true; }
        if(arguments.read(1, "--music-exit")) { exitOnMusicStop = true; }
        if(arguments.read(1, "--no-music-exit")) { exitOnMusicStop = false; }
        if(arguments.read(1, "--screen", setupOnScreenNum)) {}

        if(arguments.read(1, "-m", soundtrack_filename)) {}

        // --demo overrides the above
        string demolua, demomsx;
        if(arguments.read(1, "--demo", demolua, demomsx)) {
            // Don't override -c
            optRun = demolua;
            optCamOn = false;
            startBlack = true;
            exitOnMusicStop = true;
            setupOnScreenNum = 0;
            soundtrack_filename = demomsx;
        }

        // -v: can be specified multiple times.  The first one turns on
        // CLIThread::show_warnings.  Each additional one increases the notify
        // level by 1.
        if(arguments.read(1, "-v")) {
            if(CLIThread::show_warnings && getNotifyLevel() < DEBUG_FP) {
                setNotifyLevel((NotifySeverity)(getNotifyLevel()+1));
            }
            CLIThread::show_warnings = true;
        }

        // After we've checked for options, if there is an option left
        // that we don't recognize, exit the loop.  Assume those arguments
        // are destined for Lua.
        if(arguments.isOption(1)) {
            const ApplicationUsage::UsageMap& valid_opts(
                arguments.getApplicationUsage()->getCommandLineOptions());

            // We have to search for a prefix because valid_opts holds
            // the full first parameter to
            // ApplicationUsage::addCommandLineOption as the key.
            // Adapted from https://stackoverflow.com/a/9350066/2877364 by
            // https://stackoverflow.com/users/533120/branko-dimitrijevic .
            auto it = valid_opts.cbegin();
            for( ; it != valid_opts.cend() ; ++it ) {
                const string key = it->first;

                // Check for identity
                if(key == arguments[1]) {
                    break;  // out of the inner loop with it != cend
                }

                // Check for prefix, which must be followed by a space.
                // This avoids matching "--foo" against "--foobar".
                string arg(arguments[1]);
                arg += ' ';
                if(key.compare(0, arg.length(), arg) == 0) {
                    break;  // out of the inner loop with it != cend
                }
            }

            if(it == valid_opts.cend()) {
                break; // out of the outer arguments.containsOptions() loop
            }

        } //endif arguments[1] is an option

    } //while containsOptions

    // Postprocess ----------------------------------------------------

    if(CLIThread::show_warnings) {
        // If you are asking for -v, make it easier to find where the
        // output started.
        cout << "\n\n\n\n\n\n";
        cout << "####################################################" << endl;
        cout << "####################################################" << endl;

        // Print the notify level in a user-friendly way
        const char* NotifyLevelNames[] = {
            "ALWAYS",
            "FATAL",
            "WARN",
            "NOTICE",
            "INFO",
            "DEBUG_INFO",
            "DEBUG_FP"
        };

        auto notifyLevel = getNotifyLevel();
        char buf[256];
        snprintf(buf, 256, "%d", notifyLevel);

        cout << "Showing Lua output; notify level "
            << ( (notifyLevel >= ALWAYS && notifyLevel <= DEBUG_FP) ?
                    NotifyLevelNames[notifyLevel] :
                    buf
               )
            << endl;
    } //endif -v

    OSG_INFO << "## Arguments:" << endl;
    OSG_INFO << "## optRun " << optRun << endl;
    OSG_INFO << "## optCamOn " << optCamOn << endl;
    OSG_INFO << "## soundStream " << soundStream << endl;
    OSG_INFO << "## soundtrackFilename " << soundtrack_filename << endl;
    OSG_INFO << "## startBlack " << startBlack << endl;
    OSG_INFO << "## exitOnMusicStop " << exitOnMusicStop << endl;
    OSG_INFO << "## viewerConfigFilename " << viewerConfigFilename << endl;
    OSG_INFO << "## setupOnScreenNum " << setupOnScreenNum << endl;

    // report any errors if they have occurred when parsing the program arguments.
    bool has_err(arguments.errors());
    if(has_err)
    {
        arguments.writeErrorMessages(osg::notify(NotifySeverity::WARN));
    }

    // Load the soundtrack
    if(!soundtrack_filename.empty()) {
        OSG_NOTICE << "Trying to load music " << soundtrack_filename << endl;
        if(loadSoundtrack(soundtrack_filename)) {
            OSG_WARN << "Couldn't load music " << soundtrack_filename << endl;
            return 1;
        } else {
            cout << "Loaded music " << soundtrack_filename << endl;
            //loadSoundtrack sets soundtrackFilename = soundtrack_filename
        }
    }

    // If there's an optRun, assume it will handle the camera.
    CamManipCmd = (!optRun.empty() && !optCamOn) ? TurnOff : NoCamCmd;
        // NoCamCmd because we are currently always loading the
        // manipulator on startup.

    // If there are any parameters left, tack them on the front of the
    // argument list to Lua.  Yes, this is the slow way :) .
    for(int argidx = inner_argc-1; argidx>0; --argidx) {
        remainingArgs.push_front(inner_argv[argidx]);
    }

    // Cleanup --------------------------------------------------------
    for(vector<char *>::iterator it=to_free.begin(); it!=to_free.end(); ++it) {
        delete[] *it;
    }
    to_free.clear();
    delete[] inner_argv;

    return (has_err ? 1 : 0);
} //ParseArguments

// }}}1
// Main /////////////////////////////////////////////////////////////// {{{1

int innards(int argc, char** argv)
{
    // Argument parsing -------------------------------------- {{{2
    cout << "lua-osg-livecoding main()" << endl;
    int exitcode = ParseArguments(argc, argv);
    if(exitcode != 0) return exitcode;

    // }}}2
    // Init -------------------------------------------------- {{{2

    // Start up threading
    Thread::Init();

    cout << "main() running in thread " << pthread_self() << endl;

    // Start up audio
    Au_Startup();   // idempotent, so OK to call multiple times.

    StringQ command_queue;
    ResponseQ response_queue;

    // Load scripting support
    do { //once
        osg::ref_ptr<osgDB::ObjectWrapperManager> owm(
                osgDB::Registry::instance()->getObjectWrapperManager());
        if(!owm.valid()) {
            OSG_WARN << "Could not load OWM - scripting extras not available" << endl;
            break;
        }

        if(!owm->loadWrapperLibraries("scripting_osg")) {
            OSG_WARN << "Could not load scripting_osg" << endl;
        }

        if(!owm->loadWrapperLibraries("scripting_osgTextBase")) {
            OSG_WARN << "Could not load scripting_osgTextBase" << endl;
        }

        if(!owm->loadWrapperLibraries("scripting_osgText")) {
            OSG_WARN << "Could not load scripting_osgText" << endl;
        }

    } while(0);

    // Create the LuaScriptEngine.
#ifdef CONCRETE_LUA
    // readRefObject always returns ref_ptr<Object>, so you need an
    // extra dynamic_cast.  readRefFile<T>() returns ref_ptr<T> directly.
    //
    // Note: "ScriptEngine.lua" is a fake filename.  osgDB recognizes
    // the "ScriptEngine" part as a cue to create a ScriptEngine
    // rather than loading a file from disk.
    cout << "Creating concrete LuaScriptEngine" << endl;
    ref_ptr<lua::LuaScriptEngine> lse =
        osgDB::readRefFile<lua::LuaScriptEngine>("ScriptEngine.lua");

    registerCFunctions(lse);

#else
    cout << "Creating abstract ScriptEngine" << endl;
    ref_ptr<ScriptEngine> lse =
        osgDB::readRefFile<ScriptEngine>("ScriptEngine.lua");
#endif //CONCRETE_LUA else

    if(!lse) {
        cout << "Could not create script engine with readRefFile" << endl;
        return 1;
    }

    CLIThread clithread(command_queue, response_queue);
    clithread.start();

    // }}}2
    // Viewer init ------------------------------------------- {{{2

    // Viewer in main thread per http://forum.openscenegraph.org/viewtopic.php?t=9520
    int retval = 1;     // error exit if we break; set to 0 at the end of the do
    do {    // The scope of the viewer, in a do block so we can use `break`
            // to take us to the cleanup code.
        ref_ptr<osgViewer::Viewer> viewer(new osgViewer::Viewer());

        if(!viewerConfigFilename.empty()) {
            OSG_NOTICE << "## Using config file " << viewerConfigFilename << endl;
            setWindowConfiguration(viewer, viewerConfigFilename);
        } else if(setupOnScreenNum != -1) {
            OSG_NOTICE << "## Using screen " << setupOnScreenNum << endl;
            viewer->apply(new osgViewer::SingleScreen(setupOnScreenNum));
        }

        printOpenGLVersionAndRealize(viewer);

        viewer->getCamera()->setName("main camera");
        viewer->getCamera()->setClearColor(osg::Vec4(0,0,0,1));
        viewer->setQuitEventSetsDone(true);

        ref_ptr<osg::Group> sceneData(new osg::Group());
        viewer->setSceneData(sceneData);

        // add the help handler
        viewer->addEventHandler(new osgViewer::HelpHandler());

        // add the state manipulator
        viewer->addEventHandler(
            new osgGA::StateSetManipulator(
                                viewer->getCamera()->getOrCreateStateSet()) );

        // add the thread model handler
        viewer->addEventHandler(new osgViewer::ThreadingHandler);

        // add the window size toggle handler
        viewer->addEventHandler(new osgViewer::WindowSizeHandler);

        // add the stats handler
        viewer->addEventHandler(new osgViewer::StatsHandler);

        // add the Lua handler, which also loads print_r.lua and cpp-interface.lua
        ref_ptr<RunLuaHandler> lua =
                new RunLuaHandler(lse.get(), command_queue, response_queue);
        viewer->addEventHandler(lua);

        // copy command-line arguments into Lua
        loadLuaArgv(lua, remainingArgs);
        if(!optRun.empty()) {
            ref_ptr<StringValueObject> s(new StringValueObject(optRun));
            setLuaGlobal(lua, "LOL_RUN", s);
        }
        if(startBlack) {
            ref_ptr<BoolValueObject> b(new BoolValueObject(true));
            setLuaGlobal(lua, "NO_AXIS_MARKERS", b);
        }

        // Give the Lua script a reference to the camera
        setLuaGlobal(lua, "CAM", viewer->getCamera());

        // Set up a camera for a background behind all the rendered geometry
        {
            auto bgCamera = setupBGFG(viewer, false, true, startBlack);
                // false => post-render
                // true => the camera is a node, not a slave.  This causes it
                // to render on each screen independently.  Using `false`
                // (slave camera) causes it to render only on screen 0.
                // TODO figure this out with respect to multi-monitor.
            if(!bgCamera) break;

            setLuaGlobal(lua,"BG", bgCamera->getChild(0));
        }

        // Set up a camera for the foreground
        {
            auto fgCamera = setupBGFG(viewer, false, true, true, false, true);
                // last `false` => no content
                // last `true` => on top
            fgCamera->addChild(new Group());
            setLuaGlobal(lua,"FG", fgCamera->getChild(0));
        }


        // Give the Lua script the root node
        setLuaGlobal(lua, "ROOT", sceneData);

        // Add the iResolution global now that we have a window.
        auto iResolution = setupResolutionUniform(viewer);

        ref_ptr<Uniform> iGlobalTime(new Uniform("iGlobalTime", 0.0f));
        sceneData->getOrCreateStateSet()->addUniform(iGlobalTime);
        sceneData->addUpdateCallback(new UpdateTimeCallback(iGlobalTime));

        // Load the initial geometry in the script engine we already have,
        // rather than using readRefNodeFile (which creates a new engine).
        Parameters inparms, outparms;

        if(!execScriptFile("lua/livecoding.lua", lse, inparms, outparms)) {
            cout << "Could not run lua/livecoding.lua" << endl;
            clithread.should_exit = true;
            command_queue.put("");
            break;  // out to the cleanup code
        }

        cout << "Got " << outparms.size() << " outputs from livecoding.lua" << endl;
        ref_ptr<Node> model(makeNodeFromParams(outparms));
        sceneData->addChild(model);

        // Run the main loop
        // TODO: Set the initial camera position as a manipulator would
        //       - make the camera manipulator togglable from Lua
        const double minFrameTime = 1.0 / 60.0;   // in sec

        osg::ref_ptr<osgGA::TrackballManipulator> manip(
                new osgGA::TrackballManipulator);

        // HACK: for now, use the manipulator to set the initial camera
        if(CamManipCmd == NoCamCmd) cout << "camera manipulator on" << endl;
        viewer->setCameraManipulator(manip);
        viewer->frame();

        // }}}2
        // Start the soundtrack ---------------------------------- {{{2
        if(soundStream && !Au_Play(soundStream, soundtrackFilename.c_str())) {
            OSG_WARN << "Couldn't play " << soundtrackFilename;
            Au_Delete(soundStream);
            Au_Shutdown();
            soundStream = 0;
        }

        // Tell Lua about the music
        setLuaGlobal(lua, "MUSIC", new BoolValueObject(soundStream != 0));

        // }}}2
        // Main loop --------------------------------------------- {{{2
        while(!viewer->done())
        {
            osg::Timer_t startFrameTick = osg::Timer::instance()->tick();

            // Toggle the camera manipulator
            if(CamManipCmd == TurnOn && viewer->getCameraManipulator() == 0) {
                cout << "camera manipulator on" << endl;
                // These three lines don't work to make the manipulator
                // take on the current position - not sure why not.  TODO.
                //manip = new osgGA::TrackballManipulator(0);
                //manip->setAutoComputeHomePosition(false);
                //manip->setByMatrix(viewer->getCamera()->getViewMatrix());
                viewer->setCameraManipulator(manip);
                CamManipCmd = NoCamCmd;     // Let the Lua handler have the var back

            } else if(CamManipCmd == TurnOn) {
                cout << "camera manipulator already on" << endl;
                CamManipCmd = NoCamCmd;

            } else if(CamManipCmd == TurnOff) {
                cout << "camera manipulator off" << endl;
                viewer->setCameraManipulator(0);
                CamManipCmd = NoCamCmd;
            }

            // Special commands from Lua.  Do these after running the frame
            // because it's convenient :) .
            string cmd = lua->specialCommand();
            if(!cmd.empty()) {
                if(cmd == "rewind" && soundStream) {
                    Au_Stop(soundStream);

                    if(!Au_Play(soundStream, soundtrackFilename.c_str())) {
                        OSG_WARN << "Couldn't play " << soundtrackFilename;
                        Au_Delete(soundStream);
                        Au_Shutdown();
                        soundStream = 0;
                    }
                    setLuaGlobal(lua, "MUSIC",
                            new BoolValueObject(soundStream != 0));
                } //rewind

                else if(cmd == "keepgoing") {
                    exitOnMusicStop = false;
                }

            } //endif Lua special command

            // Run the frame
            if(soundStream) {
                double time = Au_GetTimeInPlayback(soundStream);

                // Exit if we're done, and that was requested
                if(exitOnMusicStop && time>0 && !Au_IsPlaying(soundStream)) {
                    break;
                }

                // Otherwise, run the frame.
                viewer->frame(time);
            } else {
                viewer->frame();    // OSG timebase
            }

            // Wait for the next frame
            osg::Timer_t endFrameTick = osg::Timer::instance()->tick();
            double frameTime = osg::Timer::instance()->delta_s(startFrameTick, endFrameTick);
            if (frameTime < minFrameTime) OpenThreads::Thread::microSleep(static_cast<unsigned int>(1000000.0*(minFrameTime-frameTime)));

        } //while(!done())

        // }}}2
        // Cleanup------------------------------------------------ {{{2

        if(soundStream) {
            Au_Stop(soundStream);
            Au_Delete(soundStream);
            Au_Shutdown();
            soundStream = 0;
        }
        viewer->stopThreading();
        viewer->setSceneData(0);
        retval = 0;     // successful exit
    } while(0);
    //end scope - destroy the viewer here so there isn't an inert window
    //sitting on screen

    // Clean up the CLI thread
    clithread.should_exit = true;
    cout << "Requesting exit" << endl;
    clithread.requestExit();
    cout << "Waiting for CLI thread" << endl;
    clithread.join();

    cout << endl;

    finish();

    return retval;
    cout << "leaving innards()" << endl;
    return retval;
    // clithread dtor is called as we leave innards.  It is called from
    // the context of the main thread.
    // }}}2
} //innards

int main(int argc, char **argv) // {{{2
{   // Because the standard C++ library I'm using on cygwin terminates
    // silently on an uncaught exception, rather than printing a message.
    try {
        int retval = innards(argc, argv);
        cout << "leaving main()" << endl;
        return retval;
    } catch(exception& e) {
        OSG_WARN << "Uncaught exception: " << e.what() << endl;
        return 1;
    } catch(...) {
        OSG_WARN << "There was an uncaught custom exception.\n";
        return 2;
    }
} //main }}}2

// }}}1
// Original copyrights //////////////////////////////////////////////// {{{1

// osgviewer
/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2010 Robert Osfield
 *
 * This application is open source and may be redistributed and/or modified
 * freely and without restriction, both in commercial and non commercial applications,
 * as long as this copyright notice is maintained.
 *
 * This application is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

// luaprompt
/* Copyright (C) 2012-2015 Papavasileiou Dimitris
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// }}}1
// vi: set ts=4 sts=4 sw=4 et ai fo=crql fdm=marker: //
