// lua2.h: Lua2 interface for OpenSceneGraph
// Copyright (c) 2018 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

#ifndef _LUA2_H_
#define _LUA2_H_

#include "bulk.hpp"

#ifdef CONCRETE_LUA
// Makefile adds -Isrc/osgPlugins
#include <lua/LuaScriptEngine.h>
#else
#error "define CONCRETE_LUA and recompile"
#endif // CONCRETE_LUA

void registerCFunctions(lua::LuaScriptEngine *lse);

#endif //_LUA2_H_

// vi: set ts=4 sts=4 sw=4 et ai fo=crql: //
