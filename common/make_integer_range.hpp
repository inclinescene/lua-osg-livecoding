// The Art of C++ / Sequences
// Copyright (c) 2015 Daniel Frey
//
// This version, as the original, licensed MIT.  Specifically:
//
//Copyright (c) 2015-2017 Daniel Frey and cxw42
//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation files
//(the "Software"), to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify, merge,
//publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef TAOCPP_SEQUENCES_INCLUDE_MAKE_INTEGER_RANGE_HPP
#define TAOCPP_SEQUENCES_INCLUDE_MAKE_INTEGER_RANGE_HPP

#include "make_integer_sequence.hpp"

namespace Incline
{
  namespace seq
  {
    namespace impl
    {
      template< typename T, T Begin, T Steps, bool Increase,
          T Delta = T( 1 ),
          typename = typename Incline::seq::make_integer_sequence< T, Steps >::type >
      struct generate_range;

      template< typename T, T B, T S, T D, T... Ns >
      struct generate_range< T, B, S, true, D, integer_sequence< T, Ns... > >
      {
        using type = integer_sequence< T, B + D * Ns... >;
      };

      template< typename T, T B, T S, T D, T... Ns >
      struct generate_range< T, B, S, false, D, integer_sequence< T, Ns... > >
      {
        using type = integer_sequence< T, B - D * Ns... >;
      };
    }

    template< typename T, T N, T M >
    using make_integer_range =
      typename impl::generate_range< T,
         N, //Begin
         ( N <= M ) ? ( M - N ) : ( N - M ), // Steps
         ( N <= M )   // Increase
      >::type;

    template< std::size_t N, std::size_t M >
    using make_index_range = make_integer_range< std::size_t, N, M >;

  }
}

#endif // TAOCPP_SEQUENCES_INCLUDE_MAKE_INTEGER_RANGE_HPP
// vi: set ts=2 sts=2 sw=2 et ai: //
