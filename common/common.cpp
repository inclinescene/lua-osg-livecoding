// common.cpp: Common routines for nmte/tests.
// "using namespace" is not used in this file so it's easier to move these
// routines around if necessary.

// Copyright (c) 2017--2018 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

#include "common.h"
#include <osgViewer/GraphicsWindow>
#include <osgViewer/ViewerEventHandlers>
#include <osg/Notify>

std::string doubleToString(double d)
{ // by http://stackoverflow.com/users/39375/tyler-mchenry
    // from http://stackoverflow.com/a/1314006/2877364
    std::ostringstream ss;
    ss << d;
    return ss.str();
}

std::string intToString(int i)
{ //as doubleToString
    std::ostringstream ss;
    ss << i;
    return ss.str();
}

int sernum()
{
    static int val = 0;
    return ++val;
} //sernum

std::string newtag(std::string ident)
{
    return ident + intToString(sernum());
}

void setColorOfGeode(osg::ref_ptr<osg::Geode> geode, osg::Vec4d color, bool enableLighting)
{
    osg::ref_ptr<osg::StateSet> states(geode->getOrCreateStateSet());
    osg::ref_ptr<osg::Material> material = new osg::Material;
    states->setAttributeAndModes(material, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
    material->setDiffuse(osg::Material::FRONT_AND_BACK, color);
    if(enableLighting)
        states->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
}

osg::ref_ptr<osg::Geode> drawableAt(osg::Group *parent,
    osg::Drawable *drawable, float x, float y, float z, bool suppressLighting)
{ //helper
    osg::ref_ptr<osg::PositionAttitudeTransform> pat(new osg::PositionAttitudeTransform());
    parent->addChild(pat.get());
    pat->setName(newtag("drawableAt pat"));
    pat->setPosition(osg::Vec3(x, y, z));
    //pat->setAttitude(Quat(inDegrees(90.0f), X_AXIS));

    osg::ref_ptr<osg::Geode> node(new osg::Geode());
    pat->addChild(node.get());
    node->setName(newtag("drawableAt node"));
    node->addDrawable(drawable);

    drawable->getOrCreateStateSet()->setDataVariance(osg::Object::DYNAMIC);
    node->getOrCreateStateSet()->setDataVariance(osg::Object::DYNAMIC);
        // Play it safe.  For high-speed code with fixed state sets, don't use drawableAt() :)

    if (suppressLighting) {
        node->getOrCreateStateSet()->setMode(GL_LIGHTING,
            osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);
    }
    return node;
} //drawableAt(Group, Drawable, ...)

osg::ref_ptr<osg::Geode> drawableAt(osg::Group *parent,
    osg::Shape *shape, float x, float y, float z, bool suppressLighting)
{ //helper
    osg::ref_ptr<osg::ShapeDrawable> sd(new osg::ShapeDrawable(shape));
    return drawableAt(parent, sd.get(), x, y, z, suppressLighting);
} //drawableAt(Group, Shape, ...)

osg::ref_ptr<osgText::Text3D> makeText3D()
{ //adapted from examples/osgtext
    osg::ref_ptr<osgText::Text3D> text(new osgText::Text3D);
    text->setDrawMode(osgText::Text3D::TEXT);
    text->setAxisAlignment(osgText::Text3D::XZ_PLANE);

    osg::ref_ptr<osgText::Font> font(osgText::readFontFile("c:/windows/fonts/times.ttf"));
    text->setFont(font.get());

    osg::ref_ptr<osgText::Style> style(new osgText::Style());
    text->setStyle(style.get());
    style->setThicknessRatio(0.1f);

    osg::Vec4 layoutColor(1.0f, 1.0f, 0.0f, 1.0f);  //yellow by default
    text->setColor(layoutColor);

    const float layoutCharacterSize = 1.0f;
    text->setCharacterSize(layoutCharacterSize);

    return text;
} //makeText3D

osg::ref_ptr<osgText::Text> makeText()
{ //adapted from examples/osgtext
    osg::ref_ptr<osgText::Text> text(new osgText::Text);
    text->setDrawMode(osgText::Text::TEXT);
    text->setAxisAlignment(osgText::Text::XZ_PLANE);

    osg::ref_ptr<osgText::Font> font(osgText::readFontFile("c:/windows/fonts/times.ttf"));
    text->setFont(font.get());

    osg::ref_ptr<osgText::Style> style(new osgText::Style());
    text->setStyle(style.get());
    style->setThicknessRatio(0.1f);

    osg::Vec4 layoutColor(1.0f, 1.0f, 0.0f, 1.0f);
    text->setColor(layoutColor);

    const float layoutCharacterSize = 1.0f;
    text->setCharacterSize(layoutCharacterSize);

    return text;
} //makeText

void createTimeReadout(osg::Group *parent, osg::ref_ptr<osgViewer::Viewer> viewer)
{ //make a shape that shows the time

    typedef osgText::Text3D TextType;     //Text or Text3D

    class TimeReadoutAnimator : public osg::Callback {
    private:
        osg::ref_ptr<osgViewer::Viewer> viewer_;
        osg::ref_ptr<TextType> text_;

    public:
        TimeReadoutAnimator(osg::ref_ptr<osgViewer::Viewer> the_viewer, osg::ref_ptr<TextType> text) :
            viewer_(the_viewer), text_(text) { }

        virtual bool run(Object* object, Object* data) {
            auto fs = viewer_->getFrameStamp();
            tm calendarTime;
            fs->getCalendarTime(calendarTime);

            double time = fs->getSimulationTime();  //the main one we use
            // Show all the times.
            // Simulation is what we set,
            // Reference is wall-clock time,
            // and calendar time doesn't seem to be set.
            std::string newtext =
                std::string("Sim: ") + doubleToString(time) + "\n" +
                "Frame: " + doubleToString(fs->getFrameNumber()) + "\n" +
                "Ref: " + doubleToString(fs->getReferenceTime()) + "\n" +
                "Cal: " + intToString(calendarTime.tm_hour) + ":" +
                            intToString(calendarTime.tm_min) + ":" +
                            intToString(calendarTime.tm_sec);
            //newtext = text_->getText().createUTF8EncodedString() + "x";
            //newtext = "foo";
            text_->setText(newtext);
            return osg::Callback::run(object, data);
        } //run

    }; //TimeReadoutAnimator

    osg::ref_ptr<TextType> text = makeText3D();  //or makeText()
    text->setName(newtag("text"));
    text->setDataVariance(osg::Object::DYNAMIC);
        //required for StateSets and Drawables that change in the update
        //traversal: see http://forum.openscenegraph.org/viewtopic.php?t=13539

    osg::ref_ptr<osg::Group> node(new osg::Group());
    parent->addChild(node.get());
    node->setName(newtag("text geode"));
    drawableAt(node.get(), text.get(), 0, 0, -2);

    node->addUpdateCallback(new TimeReadoutAnimator(viewer, text));
} //createTimeReadout

void die(const char *fmt, ...)
{
    char temp[4096];
    va_list va;
    va_start(va, fmt);
    vsnprintf(temp, sizeof(temp), fmt, va);
    va_end(va);

#if !defined(_WIN32) || defined(_CONSOLE)
    fprintf(stderr, "*** error: %s\n", temp);
#else
    MessageBox(NULL, temp, NULL, MB_OK | MB_ICONERROR);
#endif

    exit(EXIT_FAILURE);
}

void attachUpdateRoutine(osg::Node *node, UpdateRoutine update_routine, void *user_data)
{ //Set up update_routine to be called on _node_.

    class UpdateRoutineAnimator : public osg::Callback {
    private:
        UpdateRoutine update_routine_;
        void *user_data_;

    public:
        UpdateRoutineAnimator(UpdateRoutine ur, void *ud) :
            update_routine_(ur), user_data_(ud) { }

        virtual bool run(Object* object, Object* data) {
            osg::Node *n = dynamic_cast<osg::Node*>(object);
            osg::NodeVisitor* nv = dynamic_cast<osg::NodeVisitor*>(data);
            update_routine_(n, nv, user_data_);
            return osg::Callback::run(object, data);
        } //run

    }; //UpdateRoutineAnimator

    node->addUpdateCallback(new UpdateRoutineAnimator(update_routine, user_data));
} //attachUpdateRoutine

// Color spaces
static osg::Vec3d vec3_from_vec4(osg::Vec4 in)
{   //convenience extractor
    return osg::Vec3d(in.r(), in.g(), in.b());
} //vec3_from_vec4

osg::Vec3d hsv2rgb(osg::Vec3d hsv)  // NOT YET TESTED!
{    // From http://www.poynton.com/PDFs/coloureq.pdf , sec.9.1.  TODO find a faster version
    double R, G, B; //outputs
    double H = hsv[0], S = hsv[1], V = hsv[2];

    double hex = H * 6.0;     //[0,1]->[0,360/60]
    int primary = (int)floor(hex);
    int secondary = hex - primary;
    double a = (1.0 - S)*V;
    double b = (1.0 - (S*secondary))*V;
    double c = (1.0 - (S*(1.0 - secondary)))*V;

    switch (primary) {
    case 0: R = V; G = c; B = a; break;
    case 1: R = b; G = V; B = a; break;
    case 2: R = a; G = V; B = c; break;
    case 3: R = a; G = b; B = V; break;
    case 4: R = c; G = a; B = V; break;
    case 5: R = V; G = a; B = b; break;
    } //primary

    return osg::Vec3d(R, G, B);
} //hsv2rgb(Vec3d)

osg::Vec4d hsv2rgb(osg::Vec4d hsva)
{   //leaves a unchanged
    osg::Vec3d rgb = hsv2rgb(vec3_from_vec4(hsva));
    return osg::Vec4d(rgb, hsva.a());
} //hsv2rgb(Vec4d)

// Cameras

osg::ref_ptr<osg::Camera> createHUDCamera(double left, double right,
    double bottom, double top, bool detach /* = true*/)
{   // Modified from osghud.  See explanations at
    // https://www.packtpub.com/books/content/openscenegraph-advanced-scene-graph-components .
    // Parameters are the coordinates of the screen, e.g., 0,width,0,height.

    // create a camera to set up the projection and model view matrices, and the subgraph to draw in the HUD
    osg::ref_ptr<osg::Camera> camera(new osg::Camera);

    // set the projection matrix
    camera->setProjectionMatrix(osg::Matrix::ortho2D(left, right, bottom, top));

    // set the view matrix
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    // Parent transforms don't affect this camera
    camera->setViewMatrix(osg::Matrix::identity());

    if (detach) {
        // only clear the depth buffer
        camera->setClearMask(GL_DEPTH_BUFFER_BIT);

        // draw subgraph after main camera view.
        camera->setRenderOrder(osg::Camera::POST_RENDER);

        // we don't want the camera to grab event focus from the viewer's main camera(s).
        camera->setAllowEventFocus(false);
    }

    return camera;
} //createHUDCamera

bool getWindowDimens(osgViewer::Viewer *viewer, int& thewid, int& theht)
{
    osgViewer::Viewer::Windows wins;    // vector of GraphicsWindow*
    viewer->getWindows(wins);
    if (1 != wins.size()) return false; // EXIT POINT

    // Get the size of the window
    int x, y, wid, ht;
    osg::ref_ptr<osgViewer::GraphicsWindow> w(wins[0]);
    w->getWindowRectangle(x, y, wid, ht);
    thewid = wid;
    theht = ht;
    return true;
}

// Set up the HUD camera to take into account resizes of the viewer's window
bool attachHUDCameraToViewer(osg::Camera *hud, osgViewer::Viewer *viewer)
{
    osgViewer::Viewer::Windows wins;    // vector of GraphicsWindow*
    viewer->getWindows(wins);
    if (1 != wins.size()) return false; // EXIT POINT

    // Get the size of the window
    int x, y, wid, ht;
    osg::ref_ptr<osgViewer::GraphicsWindow> w(wins[0]);
    w->getWindowRectangle(x, y, wid, ht);

    // set up the HUD accordingly
    hud->setProjectionMatrix(osg::Matrix::ortho2D(0, wid, 0, ht));

    // Make a resizer.  If you don't do this, everything will stretch
    // if you resize the window.
    viewer->addEventHandler(new TResizeHUDCameraHandler(w.get(), hud));

    return true;
} //attachHUDCameraToViewer

bool TResizeHUDCameraHandler::handle(osgGA::Event* event
        , osg::Object* // object
        , osg::NodeVisitor* // nv
    )
{
    osgGA::GUIEventAdapter *ea;
    // Do we want it?  If so, do our camera and window still exist?
    if (((ea = event->asGUIEventAdapter()) != NULL) &&
        (ea->getEventType() == osgGA::GUIEventAdapter::RESIZE) &&
        cam_.valid() && win_.valid()) {
        // Note: if any crashes, use observer_ptr::lock() instead.
        int x, y, wid, ht;
        win_->getWindowRectangle(x, y, wid, ht);
        cam_->setProjectionMatrix(osg::Matrix::ortho2D(0, wid, 0, ht));
        OSG_INFO << "TResizeHUDCameraHandler: now " << wid << 'x' << ht << std::endl;
        return true;    //it's been handled
    }
    return false;   //we didn't handle it
} //TResizeHUDCameraHandler::handle()

/// makeNodeFromParams.
/// Copied and tweaked from osgPlugins/lua/ReaderWriterLua.cpp:readObjectFromScript()
osg::ref_ptr<osg::Node> makeNodeFromParams(const osg::Parameters& parameters)
{
    typedef std::vector< osg::ref_ptr<osg::Node> > Nodes;
    Nodes nodes;

    for(osg::Parameters::const_iterator itr = parameters.begin();
        itr != parameters.end();
        ++itr)
    {
        osg::Node* node = dynamic_cast<osg::Node*>(itr->get());
        if (node) nodes.push_back(node);
    }

    if (nodes.empty()) return 0;

    if (nodes.size()==1)
    {
        return nodes[0];
    }

    osg::ref_ptr<osg::Group> group = new osg::Group;
    for(Nodes::iterator itr = nodes.begin();
        itr != nodes.end();
        ++itr)
    {
        group->addChild(itr->get());
    }

    return group.get();
}

/// returns true on success, false on failure
bool execScriptFile(std::string filename, osg::ScriptEngine *se,
                    osg::Parameters& inparms, osg::Parameters& outparms,
                    std::string entryPoint)
{
    osg::ref_ptr<osg::Script> script = osgDB::readRefScriptFile(filename);
    if(!script)
    {
        OSG_NOTICE << "Could not load script " << filename << std::endl;
        return false;
    }

    if(!se->run(script, entryPoint, inparms, outparms))
    {
        OSG_NOTICE << "Could not run script from " << filename;
        if(!entryPoint.empty())
            OSG_NOTICE << " at " << entryPoint;
        OSG_NOTICE << std::endl;
        return false;
    }
    return true;
} //execScriptFile

bool ResolutionResizeHandler::handle(
        const osgGA::GUIEventAdapter& ea,
        osgGA::GUIActionAdapter& ,
        osg::Object* ,
        osg::NodeVisitor*
        )
{
    if (ea.getEventType() != osgGA::GUIEventAdapter::RESIZE) return false;
    osg::Vec2f v(ea.getWindowWidth(), ea.getWindowHeight());
    osg::ref_ptr<osg::Uniform> rp;
    if (iResolution_.lock(rp)) {
        rp->set(v);
    }
    OSG_INFO << "ResolutionResizeHandler: now " << v.x() << 'x' << v.y() << std::endl;
    return false;   // Don't interfere with any other code that wants
                    // to handle this event.
} //ResolutionResizeHandler::handle()

void setWindowConfiguration(osgViewer::Viewer *viewer, std::string filename)
{   // load filename if it exists
    if (!osgDB::fileExists(filename)) {
        return;     // accept whatever the default is
    }

#ifdef _DEBUG
    auto oldNotifyLevel = osg::getNotifyLevel();
    osg::setNotifyLevel(osg::INFO);
#endif

    OSG_NOTICE << "Using viewer config " << filename << std::endl;
    viewer->readConfiguration(filename);

#ifdef _DEBUG
    osg::setNotifyLevel(oldNotifyLevel);
#endif

} //setWindowConfiguration

// vi: set ts=4 sts=4 sw=4 et ai: //
