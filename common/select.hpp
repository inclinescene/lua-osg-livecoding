// Modified by cxw42@github from
// The Art of C++ / Sequences
// Copyright (c) 2015 Daniel Frey
//
// This version, as the original, licensed MIT.  Specifically:
//
//Copyright (c) 2015-2017 Daniel Frey and cxw42
//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation files
//(the "Software"), to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify, merge,
//publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef SELECT_HPP_
#define SELECT_HPP_

#include <cstddef>
#include <type_traits>

#include "integer_sequence.hpp"

namespace Incline
{
  namespace seq
  {
    namespace impl
    {
      template< std::size_t I, typename T, T Head, T... Tails >
      struct select_impl: public select_impl<
        I-1, T,
        // drop Head
        Tails...> {};

      template<typename T, T Head, T... Tails>
      struct select_impl<0, T, Head, Tails...>:
        public std::integral_constant<T, Head>
      {};
    }  //ns impl

    template< std::size_t I, typename T, T... Ns >
    struct select
      : public impl::select_impl<I, T, Ns...>
    {};

    template< std::size_t I, typename T, T... Ns >
    struct select< I, integer_sequence< T, Ns... > >
      : select< I, T, Ns... >
    {};
  }
}

#endif // SELECT_HPP_
// vi: set ts=2 sts=2 sw=2 et ai: //
