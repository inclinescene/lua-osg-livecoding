// common.h: Header for common.cpp
// cxw/Incline
#pragma once

#include <string>
#include <ostream>
#include <sstream>
#include <vector>
#include <wchar.h>
    // for struct tm
#include <stdarg.h>

#include <osg/ShapeDrawable>
#include <osg/Shape>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>
#include <osg/Drawable>
#include <osg/Material>
#include <osg/Callback>

#include <osgText/Font>
#include <osgText/Text>
#include <osgText/Text3D>

#include <osgViewer/Viewer>

#include <osg/Math>
#include <osg/Vec3d>
#include <osg/Vec4d>

#include <osg/ScriptEngine>
#include <osgDB/ReadFile>

// Defines //

#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef M_PI
#define M_PI       3.14159265358979323846
#endif

#define PARTNUM_DEMO_OVER   (1337)
    // by incline convention, set part number to 1337 to end the demo :)

// Types //

typedef void (*UpdateRoutine)(osg::Node *node, osg::NodeVisitor *nv, void *user_data);
    // For update routines

// Utility functions //

/// A convenient helper.  I saw this pattern online: double-check the
/// dynamic cast when debugging, but then during release, assume
/// you've got what you expect.  That way you don't incur the overhead
/// of the dynamic_cast at runtime.
template<class To, class From>
inline To asserted_cast(From from)
{
    assert(dynamic_cast<To>(from));
    return static_cast<To>(from);
} //asserted_cast

void die(const char *fmt, ...);

std::string doubleToString(double d);
std::string intToString(int i);

int sernum();    // return a unique integer
std::string newtag(std::string ident);  //return a unique tag including _ident_

// OpenSceneGraph helper functions //

void setColorOfGeode(osg::ref_ptr<osg::Geode> geode, osg::Vec4d color, bool enableLighting=true);

// Attach a drawable to shape to a group.
osg::ref_ptr<osg::Geode> drawableAt(osg::Group *parent,
    osg::Drawable *drawable, float x, float y, float z,
    bool suppressLighting=false);
osg::ref_ptr<osg::Geode> drawableAt(osg::Group *parent,
    osg::Shape *shape, float x, float y, float z,
    bool suppressLighting = false);

osg::ref_ptr<osgText::Text3D> makeText3D();
osg::ref_ptr<osgText::Text> makeText();

void createTimeReadout(osg::Group *parent, osg::ref_ptr<osgViewer::Viewer> viewer);

void attachUpdateRoutine(osg::Node *node, UpdateRoutine update_routine, void *user_data);

// Color spaces
osg::Vec3d hsv2rgb(osg::Vec3d hsv);     //all components on [0,1]
osg::Vec4d hsv2rgb(osg::Vec4d hsva);    //leaves a unchanged

// Cameras

// Create a HUD camera with the specified viewport
osg::ref_ptr<osg::Camera> createHUDCamera(double left, double right,
    double bottom, double top, bool detach=true);

// Set up the HUD camera to take into account resizes of the viewer's window
bool attachHUDCameraToViewer(osg::Camera *hud, osgViewer::Viewer *viewer);
    // returns true on success

class TResizeHUDCameraHandler : public osgGA::GUIEventHandler {
    // Thanks to http://forum.openscenegraph.org/viewtopic.php?t=12846 by Alberto Luaces for ideas.

    // The window and camera of interest.  observer_ptrs don't increase the
    // reference count of what they point to, so don't create circular
    // dependencies in the scene graph.
    osg::observer_ptr<osgViewer::GraphicsWindow> win_;
    osg::observer_ptr<osg::Camera> cam_;

public:
    TResizeHUDCameraHandler(osgViewer::GraphicsWindow *win, osg::Camera *hud_cam) :
        win_(win), cam_(hud_cam) {}

    virtual bool handle(osgGA::Event* event, osg::Object* object, osg::NodeVisitor* nv);
}; //TResizeHUDCameraHandler

/// A NodeVisitor to find a node with a specific name.
/// Modified from the OSG QSG,
/// https://github.com/pmartz/osgqsg/blob/master/Examples/FindNode/FindNodeMain.cpp
class FindNamedNode : public osg::NodeVisitor
{
public:
    FindNamedNode( const std::string& name )
      : osg::NodeVisitor( // Traverse all children.
                osg::NodeVisitor::TRAVERSE_ALL_CHILDREN ),
        _name( name ) {}

    // This method gets called for every node in the scene
    //   graph. Check each node to see if its name matches
    //   out target. If so, save the node's address.
    virtual void apply( osg::Node& node )
    {
        if (node.getName() == _name)
            _node = &node;

        // Keep traversing the rest of the scene graph.
        traverse( node );
    }

    osg::Node* getNode() { return _node.get(); }

protected:
    std::string _name;
    osg::ref_ptr<osg::Node> _node;
}; //class FindNamedNode

/// Return an osg::Node from the given parameters, which must be Nodes
osg::ref_ptr<osg::Node> makeNodeFromParams(const osg::Parameters& parameters);

/// Run a script from #filename in #se
bool execScriptFile(std::string filename, osg::ScriptEngine *se,
                    osg::Parameters& inparms, osg::Parameters& outparms,
                    std::string entryPoint="");

/// Update a uniform float vec2 with the current window's size.
/// Modified from osg ResizeHandler
class ResolutionResizeHandler : public osgGA::GUIEventHandler
{
    osg::observer_ptr<osg::Uniform> iResolution_;

public:

    ResolutionResizeHandler(osg::Uniform *u) : iResolution_(u) {}

    virtual bool handle(
        const osgGA::GUIEventAdapter& ea,
        osgGA::GUIActionAdapter& ,
        osg::Object* ,
        osg::NodeVisitor*
        );

}; //class ResolutionResizeHandler

/// Load a *.view file.
void setWindowConfiguration(osgViewer::Viewer *viewer, std::string filename);

// vi: set ts=4 sts=4 sw=4 et ai fo=crql: //
