# lua-osg-livecoding binary package - Cygwin x64

This packages is hosted on scene.org.

## Running

 1. Install Cygwin, x64.  Make sure to install at least the following
    packages.  Note that X11 is not required.
    - `lua`, `w32api`, `portaudio`, `libsndfile`, `gdal`, `7-zip`
 1. Download [the release archive](https://files.scene.org/view/parties/2017/softbound17/misc/lua-osg-livecoding-bin-cygwin-x64.7z)
 1. Unzip the archive somewhere convenient, preserving directory structure (`7z x`).
 1. Fire up a shell and `cd` into the directory where you unzipped.
 1. `./runme.sh`.

To change the default size or position of the window, edit `runme.sh` and
change the `OSG_WINDOW` line.

## More information

 - General info is available at (or following links from)
  <https://demozoo.org/productions/176625/> and
  <https://www.pouet.net/prod.php?which=71351>.
 - Source is at <https://bitbucket.org/inclinescene/lua-osg-livecoding>.

### Legal

Copyright (c) 2017-2018 cxw/Incline.  CC-BY-SA 3.0.
