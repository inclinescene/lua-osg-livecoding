# lua-osg-livecoding #

This is a livecoding application you can use to render 3D graphics using
OpenSceneGraph, driven from Lua commands at the console.

I explained some of the innards at Demosplash 2017 --- see
[Inside a Demotool](https://bitbucket.org/inclinescene/demosplash-2016/src/master/InsideADemotool.pptx).

### How do I get set up? ###

Use the [binary packages](https://files.scene.org/view/parties/2017/softbound17/misc/lua-osg-livecoding-bin-cygwin-x64.7z)
posted on scene.org :) .  Follow the installation instructions in
[README-bin-cygwin-x64.md](README-bin-cygwin-x64.md).

#### From source:

 1. If you are on Windows, install cygwin (64-bit, `x86_64`, tested).
    - Make sure to include the following packages: `lua` `w32api`, `gdal`,
      `portaudio`, `libsndfile`, `7-zip`
 1. Install the [LuaRocks](https://luarocks.org) package manager.
 1. `luarocks install <module>` for each of the following modules:
    `checks`, `luafilesystem`, `penlight`, `debugger`.
 1. `git clone` [OpenSceneGraph, customized](https://github.com/cxw42/OSG/tree/cyg-native) into `./osg` and checkout branch `cyg-native`.
 1. Build OpenSceneGraph (instructions to follow later).  This step includes
    setting environment variables that will also be used when you run the
    compiled EXE.
 1. `git clone` this repo into `./lua-osg-livecoding` and change to that
    directory.
 1. `make` (by default, will use `gcc`; has also been tested with `clang`).
 1. Run `build-gcc/lua-osg-livecoding`.

### Who do I talk to? ###

* cxw42!  I'm on Pouet, Necta, CVGM, SceneSat (occasionally), or devwrench.com.
* Or submit an issue in the bug tracker here.

Other info:
<https://demozoo.org/productions/176625/> and
<https://www.pouet.net/prod.php?which=71351>.

#### Legal

Copyright (c) 2017-2018 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
mention or link to <https://bitbucket.org/inclinescene/public> and
<http://devwrench.com>.  Some files have less restrictive copyrights, so
copyright statements within the individual files override this general
statement.

The song `assets/Kevin_MacLeod_-_The_Rule.ogg` is
"The Rule" by Kevin MacLeod (<https://incompetech.com>) and is
licensed under Creative Commons: By Attribution 3.0
<http://creativecommons.org/licenses/by/3.0/>.

The font file `assets/AnonymousPro.ttf` is the Anonymous Pro font,
Copyright (c) 2009, Mark Simonson (<http://www.ms-studio.com>,
mark@marksimonson.com).
This Font Software is licensed under the
[SIL Open Font License Version 1.1 (26 February 2007)](legal/OFL.txt).
This license is also available with a FAQ at: <http://scripts.sil.org/OFL>.
