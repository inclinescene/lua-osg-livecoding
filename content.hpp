// content.hpp: class Content, for easier building and manipulating of geometry.

// Copyright (c) 2017 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

#pragma once

#ifndef _CONTENT_HPP_
#define _CONTENT_HPP_

// stdlib
#include <string>
#include <map>
#include <stdexcept>
#include <utility>
#include <iterator>
#include <tuple>
#include <type_traits>
#include <algorithm>
#include <cassert>
#include <limits>
#include <memory>
#include <type_traits>
#include <cassert>

// OpenSceneGraph
#include <osg/Notify>
#include <osg/Array>
#include <osg/Geometry>
#include <osg/Callback>
#include <osg/BoundingBox>

// OSG serializer and scripting support
// TODO? move the serializer to a separate library?
#include <osgDB/ObjectWrapper>
#include <osgDB/Serializer>
#include <osgDB/InputStream>
#include <osgDB/OutputStream>
#include <osgDB/ReadFile>
#include <osg/ValueObject>

// Compile-time sequences
#include <make_integer_sequence.hpp>
#include <make_integer_range.hpp>
#include <select.hpp>

namespace Incline {

// Basic definitions that don't depend on any of the below

/// Map from vertex names to vertex indices.  Here since it doesn't need the
/// template arguments of Content.
typedef std::map<std::string, int> VNameToIndexMap;

/// Vector of array names.  Also doesn't depend on the template arguments
/// of Content.
typedef std::vector<std::string> ArrayNames;
/// Map from array

///////////////////////////////////////////////////////////////////////
// A first test of an extractor

// Helpers for GetValueInto
namespace {
    // TODO roll NewWithout and NewWith into combined<bool,bool,newt,from>;
    // use variadic templates to permit more than one userdata item.

    /// Create a new instance, if B is true
    template<bool B, typename NewT, typename From>
    struct NewWithout { static NewT* run(From) { return 0; } };

    template<typename NewT, typename From>
    struct NewWithout<true, NewT, From> {
        static NewT* run(From v) { return new NewT(v); }
    };

    /// Create a new instance with userdata, if B is true
    template<bool B, typename NewT, typename From, typename UData>
    struct NewWith { static NewT* run(From, UData*) { return 0; } };

    template<typename NewT, typename From, typename UData>
    struct NewWith<true, NewT, From, UData> {
        static NewT* run(From v, UData *u) { return new NewT(v, u); }
    };
} //anon namespace

/// A proxy to make it easier to write GetValueVisitor classes.
/// Whatever constructors you provide in class Wrapped determine the
/// types the visitor will respond to.  UData is optional user data,
/// stored by GetValueInto and passed to the Wrapped constructor if there
/// is a constructor that takes userdata.
template<class Wrapped, typename UData = void>
struct GetValueInto: public osg::ValueObject::GetValueVisitor
{
    /// Result type
    typedef std::shared_ptr<Wrapped> result_ptr;

    /// User data provided to the GetValueInto constructor.
    UData *udata;

    /// The value we extract from the visited osg::ValueObject.
    /// A shared_ptr so you can keep it after the GetValueInto instance
    /// is destroyed.
    result_ptr result;

    GetValueInto(UData *new_udata = nullptr): udata(new_udata), result(nullptr) {}
    virtual ~GetValueInto() {}
    Wrapped *getResult() { return result.get(); }

    // Pass the userdata if there is a constructor that will accept it,
    // otherwise just pass the value.  If no constructor will accept the
    // value, do nothing.
    //
    // Don't try the no-userdata constructor if there is a userdata constructor.
#define GVI_DECLARE_TYPE(NAME, T) \
    static const bool w_userdata_##NAME = std::is_constructible< Wrapped, \
                        T, UData* >::value;\
    static const bool wo_userdata_##NAME = \
        (!w_userdata_##NAME) && \
        std::is_constructible< Wrapped, T >::value; \
    virtual void apply( T value ) { \
        if(w_userdata_##NAME) { \
            result.reset(NewWith<w_userdata_##NAME, Wrapped, T, UData>::run(value, udata)); \
        } else if(wo_userdata_##NAME) { \
            result.reset(NewWithout<wo_userdata_##NAME, Wrapped, T>::run(value)); \
        } \
    }

    GVI_DECLARE_TYPE(bool,bool)
    GVI_DECLARE_TYPE(char,char)
    GVI_DECLARE_TYPE(uchar,unsigned char)
    GVI_DECLARE_TYPE(short,short)
    GVI_DECLARE_TYPE(ushort,unsigned short)
    GVI_DECLARE_TYPE(int,int)
    GVI_DECLARE_TYPE(uint,unsigned int)
    GVI_DECLARE_TYPE(float,float)
    GVI_DECLARE_TYPE(double,double)
    GVI_DECLARE_TYPE(string, const std::string&)

    GVI_DECLARE_TYPE(Vec2b, const osg::Vec2b&)
    GVI_DECLARE_TYPE(Vec3b, const osg::Vec3b&)
    GVI_DECLARE_TYPE(Vec4b, const osg::Vec4b&)

    GVI_DECLARE_TYPE(Vec2ub, const osg::Vec2ub&)
    GVI_DECLARE_TYPE(Vec3ub, const osg::Vec3ub&)
    GVI_DECLARE_TYPE(Vec4ub, const osg::Vec4ub&)

    GVI_DECLARE_TYPE(Vec2s, const osg::Vec2s&)
    GVI_DECLARE_TYPE(Vec3s, const osg::Vec3s&)
    GVI_DECLARE_TYPE(Vec4s, const osg::Vec4s&)

    GVI_DECLARE_TYPE(Vec2us, const osg::Vec2us&)
    GVI_DECLARE_TYPE(Vec3us, const osg::Vec3us&)
    GVI_DECLARE_TYPE(Vec4us, const osg::Vec4us&)

    GVI_DECLARE_TYPE(Vec2i, const osg::Vec2i&)
    GVI_DECLARE_TYPE(Vec3i, const osg::Vec3i&)
    GVI_DECLARE_TYPE(Vec4i, const osg::Vec4i&)

    GVI_DECLARE_TYPE(Vec2ui, const osg::Vec2ui&)
    GVI_DECLARE_TYPE(Vec3ui, const osg::Vec3ui&)
    GVI_DECLARE_TYPE(Vec4ui, const osg::Vec4ui&)

    GVI_DECLARE_TYPE(Vec2f, const osg::Vec2f&)
    GVI_DECLARE_TYPE(Vec3f, const osg::Vec3f&)
    GVI_DECLARE_TYPE(Vec4f, const osg::Vec4f&)

    GVI_DECLARE_TYPE(Vec2d, const osg::Vec2d&)
    GVI_DECLARE_TYPE(Vec3d, const osg::Vec3d&)
    GVI_DECLARE_TYPE(Vec4d, const osg::Vec4d&)

    GVI_DECLARE_TYPE(Quat, const osg::Quat&)
    GVI_DECLARE_TYPE(Plane, const osg::Plane&)
    GVI_DECLARE_TYPE(Matrixf, const osg::Matrixf&)
    GVI_DECLARE_TYPE(Matrixd, const osg::Matrixd&)
    GVI_DECLARE_TYPE(BoundingBoxf, const osg::BoundingBoxf&)
    GVI_DECLARE_TYPE(BoundingBoxd, const osg::BoundingBoxd&)
    GVI_DECLARE_TYPE(BoundingSpheref, const osg::BoundingSpheref&)
    GVI_DECLARE_TYPE(BoundingSphered, const osg::BoundingSphered&)
#undef GVI_DECLARE_TYPE

}; //GetValueInto

/// Helper for using GetValueInto with an osg::ValueObject that is pointed
/// to by an osg::Object pointer.
template<class Wrapped, typename UData=void>
class UnwrapValueObject {
public:
    typedef GetValueInto<Wrapped, UData> ThisVisitor;
    typedef typename ThisVisitor::result_ptr result_ptr;

private:
    typedef std::unique_ptr<ThisVisitor> visitor_ptr;
        //unique_ptr so we don't leak if something throws

    static result_ptr internals(osg::Object *obj, ThisVisitor *visitor)
    {
        osg::ref_ptr<osg::ValueObject> parm(
                        dynamic_cast<osg::ValueObject*>(obj));

        if(!parm) {
            OSG_DEBUG << "Attempt to unwrap a non-ValueObject" << std::endl;
            return NULL;
        }

        parm->get(*visitor);

        return visitor->result;
            // which will copy the result_ptr so the result will live
            // past the lifetime of visitor.
    } //internals()

public:

    /// No-userdata version.  This way UData does not have to be
    /// default-constructible.
    static result_ptr run(osg::Object *obj)
    {
        visitor_ptr visitor(new ThisVisitor());
        return internals(obj, visitor.get());
    } //run(obj)

    /// With-userdata version
    static result_ptr run(osg::Object *obj, UData *userdata)
    {
        visitor_ptr visitor(new ThisVisitor(userdata));
        return internals(obj, visitor.get());
    } //run(obj)
}; //class UnwrapValueObject

///////////////////////////////////////////////////////////////////////

// Yet another test of an extractor

/// Common fields for a templated extractor.
template<class ResultT>
struct VOGetBase: public osg::ValueObject::GetValueVisitor
{
    typedef ResultT result_type;
    ResultT value;
    bool ok;
    VOGetBase(): ok(false) {}
};

/// Empty template base, so we can specialize it with the apply() functions
/// we need.
template<class ResultT>
struct VOGet: public VOGetBase<ResultT> {};

/// Helper for filling in the specializations
#define VOG_GET(t) virtual void apply( t v ) { value=static_cast< result_type >(v); ok=true; }
#define VOG_GETR(t) VOG_GET( const t & )

/// Specialization for Vec3d
template<> struct VOGet<osg::Vec3d>: public VOGetBase<osg::Vec3d>
{
    VOG_GETR(osg::Vec3d)
    VOG_GETR(osg::Vec3f)
};

/// Specialization for integers, with truncation
template<> struct VOGet<long int>: public VOGetBase<long int>
{
    VOG_GET(short)
    VOG_GET(unsigned short)
    VOG_GET(int)
    VOG_GET(unsigned int)
    VOG_GET(float)
    VOG_GET(double)
};

/// Specialization for strings
template<> struct VOGet<std::string>: public VOGetBase<std::string>
{
    VOG_GETR(std::string)
};

/// Templated extractor function
template<class ResultT>
bool extract(osg::Object* obj, ResultT& retval)
{
    osg::ValueObject *value_obj = dynamic_cast<osg::ValueObject*>(obj);
    if(!value_obj) return false;

    VOGet<ResultT> extractor;
    value_obj->get(extractor);
    if(!extractor.ok) return false;

    retval = extractor.value;
    return true;
}

///////////////////////////////////////////////////////////////////////
// TODO a third test of an extractor:
// extract<tylist to make visitors for>(obj, extractor_instance).
// Each visitor in tylist calls extractor_instance.do() (or something).
// The extractor_instance is constructed separately so parameters don't have
// to be jammed into the UData like in GetValueInto.

template<class WorkerT, typename Head, typename... Tails>
struct Collector: public Collector<WorkerT, Tails...>
{
    typedef Collector<WorkerT, Tails...> parent_type;
    Collector(WorkerT& theworker): parent_type(theworker) {}
    virtual void apply( Head v ) { this->worker.handle(v); }
};

/// Base class
template<class WorkerT, typename Head>
struct Collector<WorkerT, Head>:
    public osg::ValueObject::GetValueVisitor
{
    WorkerT& worker;
    Collector(WorkerT & theworker): worker(theworker) {}
    virtual void apply( Head v ) { this->worker.handle(v); }
} ;


/// Templated extractor function
template<typename... CollectTs, class WorkerT>
void collect(osg::Object* obj, WorkerT& worker)
{
    osg::ValueObject *value_obj = dynamic_cast<osg::ValueObject*>(obj);
    if(!value_obj) throw std::invalid_argument("Can't collect from a non-ValueObject");

    Collector<WorkerT, CollectTs...> collector(worker);
    value_obj->get(collector);
}

namespace {     // Internal helpers in an anonymous namespace

    // --- Prettier access to types of tuples --------------------------

    /// A helper for tuple_element_checked so we don't try to
    /// instantiate tuple_element if Num is out of range.
    /// This greatly reduces the number of error messages we get,
    /// making it easier to understand what the error is.
    template<bool b, int Num, typename T>
    struct tuple_element_if {
        typedef void type;
    };

    /// If the condition is True, instantiate tuple_element.
    template<int Num, typename T>
    struct tuple_element_if<true, Num, T> {
        typedef typename std::tuple_element<Num, T>::type
                type;
    };

    /// Template to pick an element type out of a tuple, and print a
    /// more helpful message if that element doesn't exist.
    /// Uses tuple_element_if<> to prune the error messages.
    template<int Num, int NArrays, typename T>
    struct tuple_element_checked {
        static_assert(Num < NArrays,
        "*** Can't read an attribute array number that doesn't exist ***"
        );
        typedef typename tuple_element_if< (Num < NArrays), Num, T>::type
                type;
    };

} //anon namespace

// A vertex as a whole ================================================

/// Provide access to a single vertex in a Content object.
/// Intended to hold a reference to each attribute in a vertex.
/// This class is not intended to be serialized, but is just for use
/// at runtime.
template<typename... ElemTypes>
class ContentVertex: public std::tuple<ElemTypes...> {
public:
    typedef std::tuple<ElemTypes...> tuple_type;

    /// Constructor just forwards to std::tuple.
    ContentVertex(ElemTypes... parms) : tuple_type(parms...) {}

    /// Empty virtual destructor, to permit inheritance
    virtual ~ContentVertex() {}

    /// Deleted constructor to prohibit things like "new ContentVertex()".
    ContentVertex() = delete;

    // --- Accessors ---

    /// Access a single attribute of this vertex.  The call v.a<I>() is
    /// syntactic sugar for get<I>(v), but with a syntax more consistent
    /// with the way \c Content treats attributes and vertices.
    template<int I>
    inline
    typename tuple_element_checked<I, sizeof...(ElemTypes), tuple_type>::type
    a()
    {
        return std::get<I>(*this);
    }
}; //class ContentVertex

/// An abstract Content interface (pure virtual class).
/// This class provides access to all of the functionality of Content that
/// does not rely on the concrete types of the arrays.
class IContent {
public:
    virtual void setArrayNames(const ArrayNames& names) = 0;
    virtual ArrayNames& getArrayNames() = 0;
    virtual const ArrayNames& getArrayNames() const = 0;
    virtual int getNumArrays() = 0;
    virtual bool isBuildOrAssemblyInProgress() const = 0;
    virtual void prepareProgram(osg::Program *pgm) = 0;
    virtual void reserve(int num_elements) = 0;
    virtual void resize(int num_elements) = 0;
    virtual int size() = 0;
    virtual int expand_by(int num_addl_elements) = 0;
    virtual int reserve_for(int num_addl_elements) = 0;
    virtual void dirty() = 0;
    virtual void dirty(int idx) = 0;
    virtual int findVertex(const std::string& key) = 0;
    virtual void startbuild(osg::PrimitiveSet::Mode mode) = 0;
    virtual int nextbuild(osg::PrimitiveSet::Mode mode) = 0;
    virtual int endbuild() = 0;
    virtual void startassembly(osg::PrimitiveSet::Mode mode) = 0;
    virtual int assemVertex(int key) = 0;
    virtual int assemVertex(const std::string& key) = 0;
    virtual int endassembly() = 0;

    virtual osg::Array* getArray(int idx) = 0;
    virtual void setArray(int idx, osg::Array *newarr) = 0;

    /// Make a reference to an IContent using a dynamic_cast.
    /// Because this returns a reference, failure will throw std::bad_cast
    /// rather than silently returning 0.  The recommended use of this is:
    ///
    ///     Geometry *geom;
    ///     auto& content(IContent::from(geom));
    static IContent& from(osg::Node *node) {
        return dynamic_cast<IContent&>(*node);
    }

    /// Make a pointer to an IContent using a dynamic_cast.
    /// Returns 0 on failure.
    static IContent *ptrfrom(osg::Node *node) {
        return dynamic_cast<IContent*>(node);
    }

}; //class IContent

// Internal helpers ===================================================

namespace {     // Internal helpers in an anonymous namespace

    // --- Creating arrays for use by a Content instance ---------------

    /// Function to create an instance of a given subclass of osg::Array,
    /// wrapped in a class so it can be partially specialized.
    /// Idx counts down, starting from Count, so the base case is <0,Count>.
    /// However, ArrayTypes are processed left to right.
    /// TODO? rewrite this using index_sequence?
    /// Note that the first loaded array is assumed to be the vertex
    /// positions used for visibility-based culling.
    /// \param[in] content  Optional IContent pointer --- pass 0 if none.
    /// \param[in] tuple    Required tuple into which to load the arrays
    template<int Idx, int Count, typename... ArrayTypes>
    struct createNextArray {
        static void run(IContent *content,
                        std::tuple<ArrayTypes*...>& tuple)
        {
            // Ty = array type to create
            const int curr_idx = Count - Idx;
            typedef
                typename std::tuple_element<curr_idx,
                                        std::tuple<ArrayTypes...> >::type
                Ty;
                /// Inspired by http://stackoverflow.com/a/29753388/2877364
                /// by http://stackoverflow.com/users/245265/emile-cormier

            Ty *arr = new Ty;

            //Default to bind-per-vertex, since the intended use case is
            //per-vertex attributes with Content::av<>().
            if (arr->getBinding() == osg::Array::BIND_UNDEFINED) {
                arr->setBinding(osg::Array::BIND_PER_VERTEX);
            }

            if(content) content->setArray(curr_idx, arr);
            // Save the array pointer, with its type, to the tuple
            std::get<curr_idx>(tuple) = arr;

            // Load the rest
            createNextArray<Idx - 1, Count, ArrayTypes...>::run(content,tuple);
        } //run()
    }; //createNextArray

    /// Base case
    template<int Count, typename... ArrayTypes>
    struct createNextArray<0, Count, ArrayTypes...> {
        static void run(IContent*, std::tuple<ArrayTypes*...>& )
        { } // no-op --- we're done.  Args unused, so unnamed.

    }; //createNextArray<0>

    // -- Copying arrays from another Content instance --

    /// Copy arrays from _other_ to _this_.
    /// Idx counts down, starting from Count, so the base case is <0,Count>.
    /// However, ArrayTypes are processed left to right.
    /// TODO? rewrite this using index_sequence?
    template<int Idx, int Count, typename... ArrayTypes>
    struct copyArrays {
        static void run(IContent* to,
                        std::tuple<ArrayTypes*...>& tuple_to,
                        std::tuple<ArrayTypes*...>& tuple_from,
                        const osg::CopyOp& copyop
        )
        {
            // Ty = array type to copy
            const int curr_idx = Count - Idx;
            typedef
                typename std::tuple_element<curr_idx,
                                        std::tuple<ArrayTypes...> >::type
                Ty;
                /// Inspired by http://stackoverflow.com/a/29753388/2877364
                /// by http://stackoverflow.com/users/245265/emile-cormier

            Ty *from_arr = std::get<curr_idx>(tuple_from);

            // Clone the array
            Ty *to_arr = new Ty(*from_arr, copyop);

            // Save the array to the IContent
            to->setArray(curr_idx, to_arr);

            // Save the array pointer, with its type, to the tuple
            std::get<curr_idx>(tuple_to) = to_arr;

            // Load the rest
            copyArrays<Idx - 1, Count, ArrayTypes...>::run(
                                    to, tuple_to, tuple_from, copyop);
        } //run()
    }; //copyArrays

    /// Base case
    template<int Count, typename... ArrayTypes>
    struct copyArrays<0, Count, ArrayTypes...> {
        static void run(IContent* ,
                        std::tuple<ArrayTypes*...>& ,
                        std::tuple<ArrayTypes*...>&,
                        const osg::CopyOp&
        ) { } // no-op --- we're done.  Args unused, so unnamed.

    }; //copyArrays<0>

    // --- Typed access to attribute arrays ----------------------------

    /// Shared type definitions used throughout
    template<typename... Ts>
    struct cptr {
        // These match the corresponding definitions in class Content.
        typedef typename std::tuple<Ts*...>
                ptr_tuple_type;
        typedef typename std::tuple<Ts&...>
                ref_tuple_type;
        typedef ContentVertex<typename Ts::reference...>
                elem_tuple_type;
    };

    /// Dereference each element of the provided tuple based on the
    /// index_sequence passed as an argument.
    template<typename R, typename S, int... Is>
    R tuple_dereference(S& src, Incline::seq::integer_sequence<int, Is...>)
    {
        return R( *(std::get<Is>(src))... );
    }

    /// Convert a tuple of T* to a tuple of T&.
    template<typename... Ts>
    typename cptr<Ts...>::ref_tuple_type
    ptr_to_ref_in_tuple(typename cptr<Ts...>::ptr_tuple_type src)
    {
        auto seq =
            typename Incline::seq::make_integer_sequence<int, sizeof...(Ts)>::type {};

        return tuple_dereference<
            typename cptr<Ts...>::ref_tuple_type,
            typename cptr<Ts...>::ptr_tuple_type
        >(src, seq);
    } //ptr_to_ref_in_tuple

    // - - - - - - -

    /// Access one element from each array: forward declaration
    template<typename T, typename... Ts>
    struct MakeElemsTuple {};

    /// Packs a tuple with references to the elements of each attribute array
    /// for a single vertex.
    template<int... Is, typename... Ts>
    struct MakeElemsTuple<
        Incline::seq::integer_sequence<int, Is...>,
        Ts...
    > {
        typedef typename cptr<Ts...>::elem_tuple_type
                ret_ty;     // holds refs

        static ret_ty run(
                typename cptr<Ts...>::ptr_tuple_type src, int elemidx)
        {
            return ret_ty( (*std::get<Is>(src))[elemidx] ... );
        }
    }; //MakeElemsTuple

    // --- Loading attribute arrays from flat parameter packs ----------

    /// Accessor for the number of elements (DataSize) in a concrete
    /// osg::Array (specifically, an osg::TemplateArray).
    template<typename T>
    struct ElemsInArray {};

    template<
        typename ElemT, osg::Array::Type ArrayType,
        int DataSize, int DataType
    >
    struct ElemsInArray<
        osg::TemplateArray<ElemT, ArrayType, DataSize, DataType>
    >{
        static const int NElems = static_cast<int>(DataSize);
    };

    // - - - - - - -

    /// Sum all the element counts for the given array.
    /// This is the base case, which will only be invoked on an empty list.
    /// Thanks to http://eli.thegreenplace.net/2014/variadic-templates-in-c/
    /// for the idea of this type of base/recursive split.
    template<typename...Ts>
    struct TotalElems {
        static const int NTotalElems = 0;
    };

    /// Recursive case - will be invoked as long as there is a T parameter
    template<typename T, typename...Ts>
    struct TotalElems<T, Ts...> {
        static const int NTotalElems =
            ElemsInArray<T>::NElems +
            TotalElems<Ts...>::NTotalElems;
    };

    // - - - - - - -

    /// Make a new instance from specified parameters given in a tuple.
    template<
        typename NewT,      // what we are making
        typename TupleT,    // the tuple holding the parameters
        int... Is        // which parameters to pass to NewT()
    >
    NewT make_T(const TupleT& input, NewT*,
                Incline::seq::integer_sequence<int, Is...>)
    { //NewT* and index_sequence function args are just for deducing types
        return NewT(std::get<Is>(input)...);
    }

    // - - - - - - -

    /// Flag for SafeGetElem
    const int SGE_invalid_index = 0xffffffff;

    /// Get the indicated value in a sequence, or 0xffffffff if the index is
    /// off the end.
    template<bool OK, int Idx, typename Seq>
    struct SafeGetElem {
        static const int value = SGE_invalid_index;
    };

    template<int Idx, typename Seq>
    struct SafeGetElem<true, Idx, Seq> {
        static const int value = Incline::seq::select<Idx,Seq>::value;
    };

    // - - - - - - -

    // Note: this may have lots of temporary copies - I'm not sure.
    // TODO make this use emplace_back instead?

    /// Base case (done==true; done==false is specialized below)
    template<
        bool Done,
        int ArraySeqIdx,     // index in ArraySeq
        typename ArraySeq,      // a Incline::seq::integer_sequence<int>
        typename ContentType,
        int FirstParamIdx,
        typename ... ParamTs
    >
    struct LoadElements {
        /// Nothing left to do, so don't do anything :)
        static void load(const int ,
                         const typename ContentType::tuple_parrays_type& ,
                         const std::tuple<ParamTs...>& ) {}
    }; //LoadElements<false>

    /// Recursively load elements of _dest_ with the appropriate
    /// number of parameters from _params_.
    /// For any given iteration, load array # ArrayIdx in an instance of
    /// class Content given by ContentType, using parameters beginning
    /// at FirstParamIdx.
    template<
        int ArraySeqIdx,
        typename ArraySeq,
        typename ContentType,
        int FirstParamIdx,
        typename ... ParamTs
    >
    struct LoadElements<
        false, ArraySeqIdx, ArraySeq, ContentType, FirstParamIdx, ParamTs...
    > {
        static const int NArrays = ContentType::NArrays;
        static const int NParams = sizeof...(ParamTs);

        /// Load values into the arrays pointed to by _dest_.
        /// The _dest_ tuple itself is const, but the arrays it
        /// points to are not.
        /// Precondition: space for element index_in_array has already
        /// been allocated in each array in dest.
        static void load(const int index_in_array,
                const typename ContentType::tuple_parrays_type& dest,
                const std::tuple<ParamTs...>& params
        ) {

            // Get the array index we want
            const int ArrayIdx =
                Incline::seq::select<ArraySeqIdx, ArraySeq>::value;

            /// Get the concrete MixinVector<T> we want
            typedef typename ContentType::template array_type<ArrayIdx>::type
                    this_array_type;

            // Where we are going to put the element
            this_array_type *pArray = std::get<ArrayIdx>(dest);

            /// What type of element we need to create
            typedef typename this_array_type::value_type
                    this_element_type;

            // How many parameters we need
            const int NParamsReqd =
                ElemsInArray<this_array_type>::NElems;

            const int NextFirstParamIdx = FirstParamIdx + NParamsReqd;
                // Next iteration, we will start here

            // Make the compile-time sequence of elems we want
            auto seq_wanted =
                typename Incline::seq::make_integer_range<
                    int, FirstParamIdx, NextFirstParamIdx> {};

            // Unpack the appropriate part of the parameter tuple
            // into a new instance
            this_element_type new_element =
                make_T(params,
                        static_cast<this_element_type*>(NULL),
                        seq_wanted);

            // Save the new instance to the array
            (*pArray)[index_in_array] = new_element;

            // Set up the recursion to load the rest.
            // First, check the next array, with bounds checking so we
            // don't call Incline::seq::select<> with an invalid index
            // (that is a compile-time error).
            const int NextArrayIdx =
                SafeGetElem<
                    ( (ArraySeqIdx+1) >= 0 ) &&
                    ( (ArraySeqIdx+1) < ArraySeq::nelems ),
                    ArraySeqIdx+1,
                    ArraySeq
                >::value;

            const bool Done = (
                (NextFirstParamIdx >= NParams) ||   //ran out of parms
                (NextArrayIdx >= NArrays) ||        //ran out of arrays
                (NextArrayIdx == SGE_invalid_index) //invalid index
            );

            // Recurse.  Don't need to bounds-check ArraySeqIdx here
            // because we already did so in the computation of NextArrayIdx.
            LoadElements<
                Done, ArraySeqIdx+1, ArraySeq, ContentType, NextFirstParamIdx,
                ParamTs...
            >::load(index_in_array, dest, params);

        } //load()
    }; //LoadElements<false> (recursive case)

    // - - - - - - -
    // Copy attributes between vertices attr-by-attr

    /// Copy attribute ArrayIdx from one vertex to another.
    /// Base case: done==true.  Done==false is specialized below.
    template<bool Done, int ArrayIdx, typename ContentType>
    struct CopyVertexAttrs {
        static void copy_from_to(ContentType* ,
                int , int ) {}
    }; //CopyVertexAttrs<true>

    /// Recursive case: done==false
    template<int ArrayIdx, typename ContentType>
    struct CopyVertexAttrs<false, ArrayIdx, ContentType> {
        static const int NArrays = ContentType::NArrays;
        static void copy_from_to(ContentType* content,
                int vfrom, int vto)
        {
            // Copy this one
            content->template av<ArrayIdx>(vto) = content->template av<ArrayIdx>(vfrom);

            // Recurse to load the rest.
            const bool Done = ( (ArrayIdx+1) == NArrays );  //ran out of arrays

            CopyVertexAttrs<Done, ArrayIdx+1, ContentType>::
                copy_from_to(content, vfrom, vto);
        }
    }; //CopyVertexAttrs<false>

} //anon namespace

// Exceptions =========================================================

/// Reports situations in which some arrays have different lengths
/// than others, so we can't perform a requested operation.
class UnevenLengthsException: public std::logic_error {
public:
    explicit UnevenLengthsException()
        : std::logic_error("Arrays have different lengths") {}
}; //UnevenLengthsException

/// Reports invalid uses of the Content build and assemble functions.
class InvalidBuildException: public std::logic_error {
public:
    explicit InvalidBuildException(const char *msg)
        : std::logic_error(msg) {}
}; //InvalidBuildException

/// Reports failures related to Content::UpdateCallback
class UpdateCallbackException: public std::runtime_error {
public:
    explicit UpdateCallbackException(const char *msg)
        : std::runtime_error(msg) {}
    explicit UpdateCallbackException(const std::string& msg)
        : std::runtime_error(msg) {}
}; //UpdateCallbackException

// Main class =========================================================

// Forward declaration of the serializer wrapper
template<typename... ArrayTypes>
class ContentSerializer;

/// A subclass of osg::Geometry (thus a Drawable and a Node)
/// with more flexible accessors.  It also provides named vertices.
template<typename... ArrayTypes>
class Content : public osg::Geometry, public IContent
{
    // Types and helpers -------------------------------
public:
    static const int NArrays = sizeof...(ArrayTypes);
    typedef std::tuple<ArrayTypes ...>
            tuple_arrays_type;
    typedef typename cptr<ArrayTypes...>::ptr_tuple_type
            tuple_parrays_type;
    typedef typename cptr<ArrayTypes...>::ref_tuple_type
            tuple_rarrays_type;
    typedef osg::ref_ptr<Content<ArrayTypes...> >
            ref_ptr;
    typedef osg::observer_ptr<Content<ArrayTypes...> >
            observer_ptr;

    template<int ArrayIdx>
    struct array_type {
        typedef typename tuple_element_checked<
                    ArrayIdx, NArrays, tuple_arrays_type
                >::type
                type;
    };

    /// A class holding references to all the attributes of a given vertex
    typedef typename cptr<ArrayTypes...>::elem_tuple_type
            whole_vertex_type;

private:

    // Instance data -----------------------------------

    /// Vertex names, for bindLocations
    ArrayNames arrayNames_;

    /// Map vertex names to vertex indices.
    /// No duplicate names allowed.
    VNameToIndexMap vname_to_index_;

    /// Tuple of pointers to the various attribute arrays.
    /// We don't need to hold ref_ptr<>s in this tuple because
    /// we will by virtue of inheriting from osg::Geometry
    /// once createNextArray does its work.
    /// The first element is actually the vertex array, not at
    /// attribute array.
    tuple_parrays_type attribute_parrays_;

    // -- Building DrawArrays ---

    /// Whether a build is in progress
    bool is_building_;

    /// First vertex in the current build, if is_building_
    int build_first_vidx_;

    /// Mode of the current build or assembly, if is_building_
    osg::PrimitiveSet::Mode build_asm_mode_;

    // -- Assembling DrawElements --

    /// Current DrawElements instance, or NULL.
    osg::ref_ptr<osg::DrawElements> assem_into_;

    // Helper functions --------------------------------

    /// Throw if the arrays are different sizes.
    /// Otherwise, returns the size.
    int same_size_or_throw() const
    {
        Content* mutable_this = const_cast<Content*>(this);
        int curr_size = mutable_this->getArray(0)->getNumElements();
        for(int idx=1; idx < NArrays; ++idx) {
            if((int)(mutable_this->getArray(idx)->getNumElements()) != curr_size)
                throw UnevenLengthsException();
        }
        return curr_size;
    } //same_size_or_throw()

    /// Get the size of the largest array
    int largest_size() const
    {
        Content* mutable_this = const_cast<Content*>(this);
        int curr_size = mutable_this->getArray(0)->getNumElements();
        for(int idx=1; idx < NArrays; ++idx) {
            curr_size =
                std::max(curr_size,
                        (int)mutable_this->getArray(idx)->getNumElements());
        }
        return curr_size;
    } //largest_size

    /// Common code for vload and newvload
    template<typename... Ts>
    void vload_inner(int idx, Ts... params)
    {
        static_assert(
            sizeof...(Ts) == TotalElems<ArrayTypes...>::NTotalElems,
            "*** Wrong number of parameters passed to a vload function ***"
        );

        LoadElements<false, 0,
            typename Incline::seq::make_integer_sequence<int, NArrays>::type,
                // load all the arrays
            Content, 0, Ts...>::load(
            idx, attribute_parrays_,
            std::make_tuple(params...)
        );
    } //vload_inner

    /// Common initialization tasks performed by constructors
    void init()
    {
        (void)serializer_;  // cause serializer_ to be instantiated

        // per http://forum.openscenegraph.org/viewtopic.php?p=18485#18485
        setUseDisplayList(false);
        setSupportsDisplayList(false);
        setUseVertexBufferObjects(true);

        // TODO?  Check state.getUseVertexAttributeAliasing() so we can specify
        // vertex data for osg but have it wind up only in attribute 0.
        // Also see osg::VertexAttribAlias.
    } //init

public:
    // Member functions --------------------------------

    // Lifecycle =================================================

    /// Main constructor.  The caller must provide the same number of
    /// names as array types.  Each array type must be an osg::Array
    /// concrete subclass, e.g., Vec2Array.
    Content(const ArrayNames& names)
        : Geometry()
        , arrayNames_()
        , vname_to_index_()
        , is_building_(false)
        , build_first_vidx_(0)
        , build_asm_mode_(osg::PrimitiveSet::POINTS)   // doesn't matter which mode - just need a default
        , assem_into_(0)
    {
        init();

        // Create empty arrays
        createNextArray<NArrays, NArrays, ArrayTypes... >::run(
                this, attribute_parrays_);

        // Set the array names, if any.
        if(names.size()>0) {
            setArrayNames(names);
        }
    } //ctor(names)

    /// Default constructor.  Creates empty arrays, but does not
    /// assign any names.
    Content() : Content(ArrayNames()) {}

    /// Copy constructor, needed by META_Object.
    /// Reminder: this is only called if the other object has the same
    /// template types we do.
    Content(const Content& other,
            const osg::CopyOp& copyop = osg::CopyOp::SHALLOW_COPY)
        : Geometry(other, copyop)
        // We have to repeat the member init because we can't also delegate
        // to Content().
        , arrayNames_()
        , vname_to_index_(other.vname_to_index_)
        , is_building_(other.is_building_)
        , build_first_vidx_(other.build_first_vidx_)
        , build_asm_mode_(other.build_asm_mode_)
        , assem_into_(other.assem_into_)
    {
        // Check if a build is in progress.  We prohibit copying while
        // actively building, since the actual content is in an
        // intermediate state at those times.  However, if you have
        // called startbuild but haven't actually added anything yet,
        // it's OK.
        //
        // We can't copy while assembling because a shallow copy would
        // make two references to an incomplete PrimitiveSet instance.

        if(other.isBuildOrAssemblyInProgress()) {
            throw InvalidBuildException(
                    "Can't copy a Content instance while assembling or while "
                    "actively building.");
        }

        OSG_DEBUG << "Copy-constructing content from '" << other.getName()
                    << '\'' << std::endl;

        init();

        // Now grab the arrays from _other_.  This may copy or link, depending
        // on the copy operation.
        copyArrays<NArrays, NArrays, ArrayTypes... >::run(
                *this,
                attribute_parrays_,
                other.attribute_parrays_,
                copyop);

        // Set the names only if they have already been assigned in _other_.
        // A Content instance always has arrays, but may not always have names.
        if(other.arrayNames_.size() > 0) {
            setArrayNames(other.arrayNames_);
        }

    } //copy ctor

    /// Empty virtual destructor, to permit inheritance
    virtual ~Content() {}

    // State functions ===========================================

    /// Check whether a build or assembly is in progress.  Builds are not
    /// considered to be "in progress" unless a vertex has actually been
    /// added.  This supports the use of startbuild() to end the last build
    /// and start the next.
    virtual bool isBuildOrAssemblyInProgress() const
    {
        if(assem_into_) return true;
        if(is_building_ &&
                (build_first_vidx_ != same_size_or_throw())) return true;
        return false;
    }

    /// Prepares a shader program for use with this Content.
    /// Call this on each program you are going to use for this Content.
    ///
    /// Specifially, this assigns the array names to locations in the
    /// shader program in the order given, so getArray(0) maps to attribute
    /// location 0 and the first name given in the constructor call.
    /// This way you can bind to attributes by name and forego
    /// layout(location=x) qualifiers in the GLSL.
    virtual void prepareProgram(osg::Program *pgm)
    {
        // TODO check binding wrt https://stackoverflow.com/q/4635913/2877364
        for(GLuint idx=0; idx<NArrays; ++idx) {
            pgm->addBindAttribLocation(arrayNames_[idx], idx);
        }
    } //prepareProgram

    // Multi-array functions =====================================

    /// Set the names of the arrays.  The provided array must have the same
    /// number of elements as there are arrays.
    ///
    /// This is also a serialization accessor.  It is redundant with the
    /// array names stored in the arrays themselves (which also get serialized)
    /// but is saved and restored separately for scripting support.
    virtual void setArrayNames(const ArrayNames& names)
    {
        // The present implementation of osgDB::VectorSerializer fills the
        // vector, the calls setArrayNames().  If this ever changes, the
        // below conditional will have to be changed.
        if(names.size() != NArrays) {
            throw std::out_of_range( "Content::setArrayNames(): Number of "
                    "names does not match number of arrays");
        }

        arrayNames_ = names;

        for(int i=0; i<NArrays; ++i) {
            getArray(i)->setName(arrayNames_[i]);
        }
    } //setArrayNames

    /// Serialization getter for the array names
    virtual ArrayNames& getArrayNames() { return arrayNames_; }

    /// Serialization getter for the array names
    virtual const ArrayNames& getArrayNames() const { return arrayNames_; }

    virtual int getNumArrays() { return NArrays; }

    virtual void reserve(int num_elements)
    {   // Runtime only, and osg::Array is sufficient.  Therefore,
        // use the osg::Geometry facilities for access.
        for(int idx=0; idx < NArrays; ++idx) {
            getArray(idx)->reserveArray(num_elements);
        }
    } //reserve()

    virtual void resize(int num_elements)
    {   // Runtime only, and osg::Array is sufficient.  Therefore,
        // use the osg::Geometry facilities for access.
        for(int idx=0; idx < NArrays; ++idx) {
            getArray(idx)->resizeArray(num_elements);
        }
    } //resize()

    /// Find the size, or throw if not all attribute arrays have the same size.
    virtual int size()
    {
        return same_size_or_throw();
    }

    /// Add more vertices to all arrays.  Returns the index of the
    /// first vertex to write into.
    virtual int expand_by(int num_addl_elements)
    {
        auto size = largest_size();
        resize(size + num_addl_elements);
        return size;    // the first new vertex
    } //expand_by

    /// Reserve space in all arrays for more vertices.
    /// Returns the index of the first vertex to write into.
    virtual int reserve_for(int num_addl_elements)
    {
        auto size = largest_size();
        reserve(size + num_addl_elements);
        return size;    // the first new vertex
    } //reserve_for

    // Dirtying arrays:
    // thanks to http://stackoverflow.com/a/36934435/2877364
    // by http://stackoverflow.com/users/878711/rickyviking
    // for the tip on needing to call dirty()/dirtyBound().

    /// Dirty all arrays
    virtual void dirty()
    {
        for(int idx=0; idx < NArrays; ++idx) {
            getArray(idx)->dirty();
        }
        dirtyBound();   // Since array 0 (positions) are dirty.
    } //dirty

    // Dirty one array
    virtual void dirty(int idx)
    {
        getArray(idx)->dirty();
        if(idx==0) {    //positions
            dirtyBound();
        }
    } //dirty(int)

    // Accessors =================================================

    // Find vertices ------------

    inline int findv(int key) const { return key; } // TODO? check bounds?

    int findv(const std::string& key) const { return vname_to_index_.at(key); }
        // throws if the key is not in the map

    /// IContent lookup method.
    virtual int findVertex(const std::string& key)
    {
        return findv(key);
    }

    // Array accessors ----------

    /// Raw accessor for the tuple of array pointers.  This way I don't
    /// have to re-implement std::get, std::tuple_element, etc.  Use at
    /// your own risk.
    tuple_parrays_type& arrays() { return attribute_parrays_; }

    /// Const raw accessor for the tuple of arrays
    const tuple_parrays_type& arrays() const { return attribute_parrays_; }

    /// Accessor for a tuple of references to the arrays, in case
    /// you want it.  Same caveats as arrays().  NOTE: returns
    /// by value, not reference.
    tuple_rarrays_type rarrays()
    {
        return ptr_to_ref_in_tuple<ArrayTypes...>(attribute_parrays_);
    } //rarrays()

    /// Compile-time, typed accessor for a single attribute array.
    /// Returns a pointer to the array.
    /// Trying to access an array not included in ArrayTypes is a
    /// compile-time error.
    template<int ArrayIdx>
    typename array_type<ArrayIdx>::type* arr()
    {
        return std::get<ArrayIdx>(attribute_parrays_);
    } //arr()

    /// Runtime, non-type-specific getter for a single array.
    /// This is provided to localize the mapping between array
    /// indices in Content and vertex or vertex attribute arrays
    /// in Geometry.
    virtual osg::Array* getArray(int idx)
    {
        return getVertexAttribArray(idx);
    } //getArray

    /// Runtime, non-type-specific setter for a single array.
    /// Leaves the binding unchanged.
    ///
    /// Note: even if we do not expressly setVertexArray(), we still get vertex
    /// data.  This is because of osg/src/osg/Geometry.cpp:1034,
    /// in void Geometry::accept(PrimitiveFunctor& functor) const,
    /// which uses vertex attribute array 0 for the vertices if
    /// no vertex array is specified.
    virtual void setArray(int idx, osg::Array *newarr)
    {
        setVertexAttribArray(idx, newarr,
            osg::Array::BIND_UNDEFINED  //use whatever binding newarr has
        );
    } //setArray

    // TODO add getArray(), setArray() by name rather than index

    // Vertex accessors ---------

    /// Typed accessor for all the attributes of a single vertex
    template<typename KeyT>
    whole_vertex_type v(KeyT key)
    {
        auto idx = findv(key);

        // Range checks
        auto size = same_size_or_throw();
            // TODO? relax this so it will work as long as each array
            // has size at least to hold idx?
        if(idx>=size) {
            throw std::out_of_range("Cannot access nonexistent vertex");
        }

        return MakeElemsTuple<
            typename Incline::seq::make_integer_sequence<int, NArrays>::type,
            ArrayTypes...
        >::run(attribute_parrays_, idx);
    } //v()

    /// Compile-time, typed accessor for a single element of a
    /// single, numbered vertex.
    /// Returns a reference to that particular element.
    /// Throws for nonexistent vertices.  Trying to access an array not
    /// included in ArrayTypes is a compile-time error.
    template<int ArrayIdx>
    typename array_type<ArrayIdx>::type::reference
    av(int vertex_idx)   // array_type<>::reference is value_type&, not
    {                       // array_type&.
        // TODO? if !_DEBUG, use [] rather than at()?
        return arr<ArrayIdx>()->at(vertex_idx);
            // at() throws if you ask for a nonexistent vertex.
    } //av(getter)

    /// Setter for a single attribute on a single vertex,
    /// without requiring specifying the name of the element type.
    /// Returns a reference to the element in case you want it.
    template<int ArrayIdx, typename... ParamTs>
    typename array_type<ArrayIdx>::type::reference
    av(int vertex_idx, ParamTs... params)
    {
        // What type of element we need to create
        typedef typename array_type<ArrayIdx>::type::value_type
                this_element_type;

        auto& retval = av<ArrayIdx>(vertex_idx);
            // must specify "auto&" - plain "auto" makes a value, not a ref

        retval = this_element_type(params...);
        return retval;
    } //av(setter)

    /// av getter by things other than int.
    /// TODO figure out how to force const T& for whatever is passed.
    template<int ArrayIdx, typename KeyT>
    typename array_type<ArrayIdx>::type::reference
    av(KeyT key)
    {
        auto idx = findv(key);
        return av<ArrayIdx>(idx);
    } // av<ArrayIdx>(string)

    /// Setter by const char*.  Can't use a template-param key type, because
    /// an integer first argument is confusable with a key if we do.
    template<int ArrayIdx, typename... ParamTs>
    typename array_type<ArrayIdx>::type::reference
    av(const char* key, ParamTs... params)
    { auto idx = findv(key); return av<ArrayIdx>(idx, params...); }

    /// Setter by std::string.  Can't use a template-param key type, because
    /// an integer first argument is confusable with a key if we do.
    template<int ArrayIdx, typename... ParamTs>
    typename array_type<ArrayIdx>::type::reference
    av(const std::string& key, ParamTs... params)
    { auto idx = findv(key); return av<ArrayIdx>(idx, params...); }

    // Bulk-loading data =========================================

    /// Load the indicated vertex with values.
    /// Values must be specified for all the attribute arrays.
    template<typename KeyT, typename... Ts>
    void vload(KeyT key, Ts... params)
    {
        auto idx = findv(key);

        // Range checks.  TODO figure out how to permit these to be bypassed
        // at caller option, e.g., after calling expand_by().
        auto size = same_size_or_throw();
        if(idx>=size) {
            throw std::out_of_range("Cannot load nonexistent vertex");
        }

        vload_inner(idx, params...);
    } //vload

    // Creating vertices =========================================

    // TODO? Mark all arrays as dirty whenever vload/newvload/dupv called?

    /// Add a new unnamed vertex, and load it with values.
    /// Values must be specified for all the attribute arrays.
    /// Returns the index of the new vertex.
    template<typename... Ts>
    int newvload(Ts... params)
    {
        auto size = same_size_or_throw();   // safety check

        // Make space for the new element.  Now _size_ is the
        // index of the element to load.
        resize(size+1);

        vload_inner(size, params...);
        return size;
    } //newvload(anonymous)

    /// Overload of newvload() with a std::string name --- a const char *
    /// string literal won't cut it.  That situation is handled by a
    /// separate overload.
    template<typename... Ts>
    int newvload(const std::string& name, Ts... params)
    {
        if(vname_to_index_.find(name) != vname_to_index_.end()) {
            throw std::runtime_error(std::string("Attempt to create vertex ")
                    + name + ", which already exists.");
        }

        auto idx = newvload(params...);
        vname_to_index_[name] = idx;
        return idx;
    } //newvload(named)

    /// Overload of newvload() with a string literal name.
    /// We need this because otherwise the required conversion from
    /// char * to std::string to call newvload(string,...) makes that
    /// overload lose out to the anonymous newvload(...).
    template<typename... Ts>
    int newvload(const char* name, Ts... params)
    {
        return newvload(std::string(name), params...);
    } //newvload(named)

    /// Duplicate a given vertex with all its attrs.
    template<typename T>
    int dupv(T key)
    {
        int idx = findv(key);

        auto size = same_size_or_throw();
        if(idx>=size) {
            throw std::out_of_range("Cannot duplicate nonexistent vertex");
        }

        //Make space.  TODO do this in one shot, without default-constructing?
        resize(size+1);

        // Do the copy
        CopyVertexAttrs<false, 0, Content>::copy_from_to(this, idx, size);

        return size;
    } //dupv

    // TODO add a routine to duplicate a given set of vertices.  E.g.,
    // for a cube, you could add the eight vertices and then duplicate
    // sets of them for each face.  You could then set the normals
    // for each face without having to respecify the other vertex attributes.

    // Creating pieces (PrimitiveSets) ===========================

    // --- Builds ---

    /// Start collecting newly-created vertices into a DrawArrays PrimitiveSet.
    /// Between startbuild() and the following nextbuild() or endbuild(),
    /// add the vertices in the order they should be drawn.
    /// After a startbuild(), another startbuild() will end the previous
    /// build before starting the next one.  Therefore, if you don't need the
    /// indices of the resulting PrimitiveSets, you can just call startbuild().
    virtual void startbuild(osg::PrimitiveSet::Mode mode)
    {
        if(assem_into_) {
            throw InvalidBuildException("Can't start a build while assembling");
        }

        if(is_building_) {      // Automatically finish off any previous build
            (void)endbuild();
        }

        // The first vertex of the build is the next one to be added, i.e.,
        // vertex # size().  This is since a build works
        // on *newly-created* vertices rather than existing vertices.
        // E.g., size==0 => vertex 0 is next to be created, thus first in
        // the build.
        is_building_ = true;
        build_first_vidx_ = same_size_or_throw();
        build_asm_mode_ = mode;
    } //startbuild()

    /// Finish one build and start the next.  Returns the index of the
    /// newly-created DrawArrays.
    virtual int nextbuild(osg::PrimitiveSet::Mode mode)
    {
        int retval;

        if(assem_into_) {
            throw InvalidBuildException("Can't call nextbuild() while assembling");
        }

        if(!is_building_) {
            throw InvalidBuildException(
                    "nextbuild() called before startbuild()");
        }

        retval = endbuild();
        startbuild(mode);

        return retval;
    } //nextbuild()

    /// Finish building a primitive set and create a corresponding DrawArrays.
    /// Returns the index of the primitive set.
    virtual int endbuild()
    {
        if(assem_into_) {
            throw InvalidBuildException("Can't call endbuild() while assembling");
        }

        if(!is_building_) {
            throw InvalidBuildException(
                    "endbuild() called before startbuild() or nextbuild()");
        }

        // Make the PrimitiveSet
        const int num_vertices = same_size_or_throw() - build_first_vidx_;
            // The last vertex of the build is size-1.  Therefore
            // size is last+1, so size-first is the same as
            // last-first+1 == the number of elems on [first,last].
        osg::ref_ptr<osg::DrawArrays> prim(
            new osg::DrawArrays(build_asm_mode_, build_first_vidx_,
                                num_vertices));
        addPrimitiveSet(prim);

        is_building_ = false;        //no build in progress
        build_first_vidx_ = 0;
        return (getNumPrimitiveSets()-1);
    } //endbuild()

    // --- Assemblies ---

    /// Start assembling a DrawElements primitive set.  Between startassembly()
    /// and endassembly(), call assem() to add an existing vertex to the
    /// primitive set.
    /// After a startassembly(), another startassembly() will end the previous
    /// assembly before starting the next one.  Therefore, if you don't need the
    /// indices of the resulting PrimitiveSets, you can just
    /// call startassembly().
    ///
    /// Requirement: Do not add to an assembly any vertices created after
    /// startassembly() is called for that assembly.  This is because
    /// startassembly() picks the data type used for the primitive set
    /// based on the number of vertices in the Content instance when
    /// startassembly() is called.
    virtual void startassembly(osg::PrimitiveSet::Mode mode)
    {
        if(is_building_) {
            throw InvalidBuildException("Can't start assembling while building");
        }

        if(assem_into_) {   // Automatically finish off any previous assembly
            (void)endassembly();
        }

        // Make the primitive set.  This assumes no new vertices will be added
        // between now and endassembly(), or at least that none of those
        // vertices will be added to the assembly.
        int sz = same_size_or_throw();
        if(sz < 256) {
            assem_into_ = new osg::DrawElementsUByte(mode);
        } else if(sz < 65535) {
            assem_into_ = new osg::DrawElementsUShort(mode);
        } else {
            assem_into_ = new osg::DrawElementsUInt(mode);
        }
    } //startassembly

    /// Add the indicated, already-existing vertex to the current assembly.
    /// Returns the index of that vertex, e.g., in case you are referring
    /// to vertices by name.
    template<class KeyT>
    int assem(KeyT key)
    {
        if(!assem_into_) {
            throw InvalidBuildException("assem() called before startassembly()");
        }
        auto idx = findv(key);
        assem_into_->addElement(idx);
        return idx;
    } //assem

    /// IContent assembler.
    virtual int assemVertex(int key)
    {
        return assem(key);
    }

    /// IContent assembler.
    virtual int assemVertex(const std::string& key)
    {
        return assem(key);
    }

    /// Finalize an assembly.  Returns its index among this instance's
    /// primitive sets.
    virtual int endassembly()
    {
        if(is_building_) {
            throw InvalidBuildException("Can't call endassembly() while building");
        }

        if(!assem_into_) {
            throw InvalidBuildException(
                            "endassembly() called before startassembly()");
        }

        addPrimitiveSet(assem_into_);
        assem_into_ = 0;
        return (getNumPrimitiveSets()-1);
    } //endassembly

    // Easier update callbacks ===================================

    /// An update callback for a specific Content type.  This is to
    /// take care of boilerplate and remove the need for so many
    /// dynamic_cast<>s.  Inherit from this and override init() and
    /// update().  Please create a separate UpdateCallback instance for
    /// each Content instance.
    template<typename UData = void>
    class UpdateCallback: public osg::Drawable::UpdateCallback
    {   //Use UpdateCallback instead of Callback because of an OSG 3.4.0 bug fixed in
        //https://github.com/openscenegraph/OpenSceneGraph/commit/
        //                          b04271f93e2fb244fc7a351af81e56eb43546b7a
    public:
        typedef UData userdata_type;

    protected:
        /// The Content instance this UpdateCallback is attached to
        osg::observer_ptr<Content> me;

        /// User data passed in from the constructor.
        UData* udata;

        /// Initialize the class.  Returns true on success, false on failure.
        /// This is so the writer of init() doesn't have to define a
        /// custom exception (but if you want to throw instead, go for it!).
        virtual bool init() { return true; }

        /// Do the actual update.
        virtual void do_update(double ,     //simulation time
                                osg::NodeVisitor *) {}

    public:
        UpdateCallback() = delete;  // Can't run without a parent pointer

        /// Constructor.  Child constructors must have the same signature
        /// and invoke this ctor if you are going to use the
        /// Content::newUpdateCallback() helper.
        UpdateCallback(Content *content, userdata_type* userdata = 0)
            : me(content)
            , udata(userdata)
        {
            if(!init()) {
                std::string msg("Couldn't initialize update callback for ");
                msg += me->getName();
                throw UpdateCallbackException(msg);
            }
        }

        /// The runner.  Derived classes should override update() rather
        /// than this method.
        virtual void update(osg::NodeVisitor *nv, osg::Drawable *)
        {
            auto fs = nv->getFrameStamp();
            double t = fs->getSimulationTime();

            do_update(t, nv);
        } //update()

    }; //class UpdateCallback

    /// Attach a child of UpdateCallback to this Content.
    template<class CallbackT, typename... ParamTs>
    CallbackT *newUpdateCallback(ParamTs... params)
    {
        osg::ref_ptr<CallbackT> cb(new CallbackT(this, params...));
        addUpdateCallback(cb);
        return cb.get();
    } //newUpdateCallback

#if 0
    /// Add a new vertex.
    /// Returns a ContentVertex that can be used to change the properties
    /// of that vertex.
    osg::ref_ptr<ContentVertex<ArrayTypes...> > vnew(std::string name = "");

    /// Access an exiting vertex by number.
    /// Returns a ContentVertex that can be used to change the properties
    /// of that vertex.
    osg::ref_ptr<ContentVertex<ArrayTypes...> > v(int idx);

    /// Access an exiting vertex by name.
    /// Returns a ContentVertex that can be used to change the properties
    /// of that vertex.
    /// If create_if_missing is true, create the vertex if it doesn't exist.
    osg::ref_ptr<ContentVertex<ArrayTypes...> > v(std::string name, bool create_if_missing = false);
#endif

    // Serialization support =====================================

    // These functions are required by osg::Object.  They are normally
    // provided by META_Object, but we have to define our own className()
    // so can't use META_Object.

    virtual osg::Object* cloneType() const { return new Content(); }

    // TODO clone()
    // virtual osg::Object* clone(const osg::CopyOp& copyop) const
    //      { return new Content(*this,copyop); }

    virtual bool isSameKindAs(const osg::Object* obj) const
        { return dynamic_cast<const Content *>(obj)!=NULL; }

    /// The library name.
    /// This is static so that class ContentSerializer can use it.
    static const std::string staticLibraryName() { return "Incline"; }

    // osg::Object access to the library name
    virtual const char* libraryName() const
        { return staticLibraryName().c_str(); }

    /// Build the class name including transformed classnames of the
    /// template parameters.  E.g., Content(osg$Vec3Array,osg$Vec2Array).
    /// This is static so that class ContentSerializer can use it.
    static const std::string staticClassName()
    {
        std::string builder("Content(");

        // Make a temporary set of arrays we can pull from.
        Content c;

        // Now build up the name
        for(int i=0; i<NArrays; ++i) {
            auto curr_array = c.getArray(i);
            builder += curr_array->libraryName();
            builder += '$';
                // an innocuous char that's not valid in a C++ identifier
            builder += curr_array->className();
            if( i != (NArrays-1) ) {
                builder += ',';
            }
        }
        builder += ')';

        return builder;
    } //className

    // osg::Object access to the class name
    virtual const char* className() const
    {
        return staticClassName().c_str();
    }

    // -- Serialization accessors --

    void setVNameToIndexMap(const VNameToIndexMap& map) { vname_to_index_=map; }
    VNameToIndexMap& getVNameToIndexMap() { return vname_to_index_; }
    const VNameToIndexMap& getVNameToIndexMap() const { return vname_to_index_; }

    // Scripting methods =========================================

    /// av(a, v[, newval]): accessor wrapping av<>()
    struct M_av: public osgDB::MethodObject {
        virtual bool run(osg::Object* objectPtr, osg::Parameters& inparms,
                            osg::Parameters& outparms) const
        {
            Content *content = dynamic_cast<Content*>(objectPtr);
            if(!content) {
                OSG_NOTICE << "M_av: can't call on a non-Content" << std::endl;
                return false;
            }

            if(inparms.size() < 2 || inparms.size() > 3) {
                OSG_NOTICE << "M_av: wrong number of parms" << std::endl;
                return false;
            }

            int attr_num=-1, vertex_num=-1;

            {   // Get attribute index
#if 0
                long int attridx;
                std::string attrname;
                if(extract(inparms[0], attridx)) {
                    if(attridx>=0 && attridx < NArrays) {
                        attr_num = attridx;
                    } else {
                        OSG_NOTICE << "Attribute index " << attridx << " doesn't exist" << std::endl;
                    }
                } else if(extract(inparms[0], attrname)) {
                    auto& names = content->getArrayNames();
                    auto where = std::find(names.begin(), names.end(), attrname);
                    if(where == names.end()) {
                        OSG_NOTICE << "Attribute name " << attrname << " doesn't exist" << std::endl;
                    } else {
                        attr_num = where - names.begin();
                    }
                }
#endif
                LookupArrayInContent worker(*content);
                try {
                    collect<int, unsigned int, long int, unsigned long int, float, double, const std::string&>(inparms[0], worker);
                } catch(std::exception& e) {
                    OSG_NOTICE << "M_av: couldn't get array index: " << e.what() << std::endl;
                    return false;
                }
                attr_num = worker.result;

                //auto array_obj =
                //    UnwrapValueObject<LookupArrayInContent, Content>::run(
                //            inparms[0].get(), content);
                //
                //if(!array_obj) {
                //    OSG_NOTICE << "M_av: couldn't get array index" << std::endl;
                //    return false;
                //}
                //attr_num = array_obj->result;
            }

            osg::Array *arr = content->getArray(attr_num);

            {   //Get vertex index
#if 0
                long int vertidx;
                std::string vertname;
                if(extract(inparms[1], vertidx)) {
                    if(vertidx>=0 && vertidx < arr->getNumElements()) {
                        vertex_num = vertidx;
                    } else {
                        OSG_NOTICE << "Vertex #" << vertidx
                                    << " of array " << attr_num
                                    << " doesn't exist" << std::endl;
                    }
                } else if(extract(inparms[1], vertname)) {
                    try {
                        vertex_num = content->findv(vertname);
                    } catch(std::out_of_range& ) {
                        OSG_NOTICE << "Vertex name " << vertname
                                    << " of array " << attr_num
                                    << " doesn't exist" << std::endl;
                    }
                }

                if(vertex_num == -1) {
                    OSG_NOTICE << "M_av: couldn't get vertex index" << std::endl;
                    return false;
                }
#endif
                LookupVertexInContent worker(*content, arr);
                try {
                    collect<int, unsigned int, long int, unsigned long int, float, double, const std::string&>(inparms[1], worker);
                } catch(std::exception& e) {
                    OSG_NOTICE << "M_av: couldn't get vertex index: " << e.what() << std::endl;
                    return false;
                }
                attr_num = worker.result;

                //auto array_obj =
                //    UnwrapValueObject<LookupVertexInContent, Content>::run(
                //            inparms[1].get(), content);
                //
                //if(!array_obj) {
                //    OSG_NOTICE << "M_av: couldn't get vertex index" << std::endl;
                //    return false;
                //}
                //vertex_num = array_obj->result;
            }

            // Dispatch
            if(inparms.size()==2)
                return getter(content, attr_num, vertex_num, outparms);
            else    //inparms.size()==3
                return setter(content, attr_num, vertex_num, inparms[2]);

        } //run

        /// outparms.push_back(content.av<attr_num>(vertex_num))
        virtual bool getter(Content* /*content*/,
                            int attr_num, int vertex_num,
                            osg::Parameters& /*outparms*/) const
        {
            OSG_NOTICE << "M_av::getter TODO: attr " << attr_num
                        << ", vertex " << vertex_num << std::endl;
            osg::ref_ptr<osg::Object> retval;

            // TODO implement this!
            return false;
        } //getter

        /// Set content.av<attr_num>(vertex_num) = *newval
        virtual bool setter(Content* /*content*/,
                            int attr_num, int vertex_num,
                            osg::ref_ptr<osg::Object>& /*newval*/) const
        {
            OSG_NOTICE << "M_av::setter TODO: attr " << attr_num
                        << ", vertex " << vertex_num << std::endl;
            // TODO implement this!
            return false;
        } //setter

    }; //M_av

protected:
    // Support classes for scripting methods =====================

    /// Get an attribute (array) index from a parameter.  Wrapped class for use
    /// with collect().
    struct LookupArrayInContent {
        int result;
        const Content& content;
        LookupArrayInContent(const Content& thecontent): content(thecontent) {}

        /// The one we use for Lua numeric literals.  In LuaScriptEngine.cpp,
        /// LuaScriptEngine::getType() returns RW_DOUBLE for any Lua
        /// number (LUA_TNUMBER).  The doubles get truncated to fit this ctor.
        void handle(double d) {
            if(!(d >= 0.0 && d < INT_MAX)) {
                throw std::invalid_argument("Index out of range");
            }
            int idx = static_cast<int>(d);
            if(idx<0 || idx>=content.NArrays) {
                std::stringstream ss;
                ss << "Array " << idx << " (" << d << ") does not exist";
                throw std::invalid_argument(ss.str());
            }
            result = idx;
        }

        void handle(const std::string& name) {
            auto& names = content.getArrayNames();
            auto where = std::find(names.begin(), names.end(), name);
            if(where == names.end()) {
                std::stringstream ss;
                ss << "Attribute name " << name << " does not exist";
                throw std::invalid_argument(ss.str());
            } else {
                result = where - names.begin();
            }
        }
    }; //class LookupArrayInContent

    /// Get a vertex index from a parameter.  Wrapped class for use
    /// with collect().
    struct LookupVertexInContent {
        int result;
        const Content& content;
        const osg::Array* arr;
        LookupVertexInContent(const Content& thecontent, const osg::Array *thearr)
            : content(thecontent)
            , arr(thearr)
        {}

        void handle(double d) {
            if(!(d >= 0.0 && d < INT_MAX)) {
                throw std::invalid_argument("Index out of range");
            }
            int idx = static_cast<int>(d);
            //check in range, and check for  vvvvvvvv  uint->int truncation
            if(idx<0 || arr->getNumElements()>INT_MAX ||
                                        idx>=(int)(arr->getNumElements())) {
                std::stringstream ss;
                ss << "Vertex " << idx << " (" << d << ") does not exist";
                throw std::invalid_argument(ss.str());
            }
            result = idx;
        }

        void handle(const std::string& name) {
            result = content.findv(name);
        }
    }; //class LookupVertexInContent
#if 0
    /// Get an attribute (array) index from a parameter.  Wrapped class for use
    /// with GetValueInto<>.
    struct LookupArrayInContent {
        int result;
        /// The one we use for Lua numeric literals.  In LuaScriptEngine.cpp,
        /// LuaScriptEngine::getType() returns RW_DOUBLE for any Lua
        /// number (LUA_TNUMBER).  The doubles get truncated to fit this ctor.
        LookupArrayInContent(int val): result(val) {}

        LookupArrayInContent(const std::string& val, Content *content)
        { result = content->getVNameToIndexMap().at(val); }
            // since I haven't implemented getArray(string) yet
    }; //class LookupArrayInContent

    /// Get a vertex index from a parameter.  Wrapped class for use
    /// with GetValueInto<>.
    struct LookupVertexInContent {
        int result;
        LookupVertexInContent(int val): result(val) {}
        LookupVertexInContent(const std::string& val, Content *content)
        { result = content->findv(val); }
    }; //class LookupVertexInContent
#endif

private:
    // Serialization wrapper ---------------------------
    static ContentSerializer<ArrayTypes...> serializer_;
}; //class Content

/// Allocate space for the static serializer.  This is OK per
/// https://stackoverflow.com/a/23690517/2877364 by
/// https://stackoverflow.com/users/241631/praetorian .
template<typename... ArrayTypes>
ContentSerializer<ArrayTypes...> Content<ArrayTypes...>::serializer_;

// Serializer for Content =============================================

/// Serializer for Content<ArrayTypes...>.
/// NOTE: Does not create the extern wrapper_serializer_*() function that
/// REGISTER_OBJECT_WRAPPER does.  This may cause issues with
/// deprecated-dotosg, but I am not sure.
///
/// The building- and assembling-related members are not serialized.  Content
/// does not support being read, written, or copy-constructed while a build or
/// assembly is in progress.
template<typename... ArrayTypes>
class ContentSerializer
{
public:
    /// The Content type we will be serializing.
    typedef Content<ArrayTypes...> content_type;
private:

    /// The RegisterWrapperProxy that will manage the interface to the Registry.
    /// Held in a unique_ptr since its lifespan must extend after the last
    /// use of the object wrapper for content_type.
    static std::unique_ptr<osgDB::RegisterWrapperProxy> proxy;

    /// Create a new instance of content_type
    static osg::Object *create_instance_func_()
    {
        return new content_type();
    }

    /// Enforce the requirement that objects not be serialized while a build or
    /// assembly is in progress.  Uses a user serializer as described at
    /// http://trac.openscenegraph.org/projects/osg/wiki/Support/KnowledgeBase/SerializationSupport#a2.3Customserializers .
    static bool checkOKToSerDes(const content_type& content)
    {
        if(content.isBuildOrAssemblyInProgress()) {
            throw InvalidBuildException(
                    "Can't read or write a Content instance while assembling "
                    "or while actively building.");
        }

        return false;       //nothing to write
    } //checkOKToSerDes

    /// No-op writer for the sake of ADD_USER_SERIALIZER
    static bool writeOKToSerDes(osgDB::OutputStream& , const content_type& )
    { return true; }    // true => everything is OK

    /// No-op reader for the sake of ADD_USER_SERIALIZER
    static bool readOKToSerDes(osgDB::InputStream& , content_type& )
    { return true; }    // true => everything is OK

    /// Populate the object wrapper for this content_type.
    /// \param wrapper  The ObjectWrapper instance --- must be named "wrapper"
    ///                 for the ADD_*_SERIALIZER macros to work.
    static void add_prop_func_(osgDB::ObjectWrapper *wrapper)
    {
        typedef content_type MyClass;   //required by ADD_*_SERIALIZER

        // -- Enforce no serializing while building or assembling --
        // Can't use ADD_USER_SERIALIZER for class members

        wrapper->addSerializer( new osgDB::UserSerializer<MyClass>(
            "OKToSerDes", &ContentSerializer::checkOKToSerDes,
            &ContentSerializer::readOKToSerDes,
            &ContentSerializer::writeOKToSerDes),
            osgDB::BaseSerializer::RW_USER );

        // -- Content-specific serializers --

        ADD_MAP_SERIALIZER(VNameToIndexMap, VNameToIndexMap,
            osgDB::BaseSerializer::RW_STRING, osgDB::BaseSerializer::RW_UINT);
            // TODO use a fixed size for the key type - int can vary

        ADD_VECTOR_SERIALIZER( ArrayNames, ArrayNames,
            osgDB::BaseSerializer::RW_UINT, 1);
            // TODO use a fixed size

        // -- Scripting methods --
        ADD_METHOD_OBJECT("av", typename content_type::M_av);

    } //add_prop_func_

public:

    /// Constructor
    ContentSerializer()
    {
        // Create the proxy if we haven't yet.  Not sure if I actually
        // need this check, but it's here just in case.
        if(!proxy) {

            // Get the data the proxy needs
            const std::string className =
                    content_type::staticLibraryName() + "::" +
                    content_type::staticClassName();

            OSG_DEBUG << "Registering wrapper for " << className << std::endl;

            const std::string associates =
                std::string("osg::Object osg::Node osg::Drawable osg::Geometry ") +
                className;

            // Create the proxy.  The proxy's ctor will register a wrapper
            // and invoke add_prop_func_ to populate it.
            proxy.reset(
                    new osgDB::RegisterWrapperProxy(
                        &create_instance_func_,
                        className,
                        associates,
                        &add_prop_func_
                    )
                );
        }
    } //ctor

}; //ContentSerializer<ArrayTypes...>

/// Allocate space for the static serializer's proxy.
template<typename... ArrayTypes>
std::unique_ptr<osgDB::RegisterWrapperProxy> ContentSerializer<ArrayTypes...>::proxy;

// Some useful functions.  TODO replace with glm where appropriate. ===

/// Load the given vertex and fragment shaders, and attach them to
/// #node's StateSet.  If #node implements IContent, also set the
/// name-to-array mapping.
/// This has to be a template to avoid redefinition errors.
template<class Unused = void>
void applyShader(osg::Node *node, std::string program_name,
        std::string vert, std::string frag,
        bool loadFromFile = false   ///< if true, vert and frag are filenames
)
{
    osg::StateSet* ss = node->getOrCreateStateSet();
    osg::ref_ptr<osg::Program> program(new osg::Program);
    program->setName(program_name);

    // Set the array names, if it's a Content instance
    {
        auto content(IContent::ptrfrom(node));
        if(content) content->prepareProgram(program);
    }

    if(loadFromFile) {
        osg::ref_ptr<osgDB::Options> options(new osgDB::Options);
        options->setObjectCacheHint(osgDB::Options::CACHE_NONE);
        osg::ref_ptr<osg::Shader> shader;

        shader = osgDB::readShaderFile(osg::Shader::VERTEX, vert, options);
            // will process #include statements! :)
        if (!shader.valid()) {
            OSG_NOTICE << " #### Could not load vertex shader " << vert << std::endl;
        } else {
            program->addShader(shader);
        }

        shader = osgDB::readShaderFile(osg::Shader::VERTEX, frag, options);
        if (!shader.valid()) {
            OSG_NOTICE << " #### Could not load fragment shader " << frag << std::endl;
        } else {
            program->addShader(shader);
        }

    } else {    //source provided inline
        program->addShader( new osg::Shader( osg::Shader::VERTEX,
                                                vert ) );
        program->addShader( new osg::Shader( osg::Shader::FRAGMENT,
                                                frag ) );
    }

    ss->setAttributeAndModes( program, osg::StateAttribute::ON );
        // don't need to turn off lighting since using a program
        // automatically switches off the fixed-function pipeline.
} //applyShader


/// clamp() --- implementation straight from the WebGL reference card
template<class FP>
FP clamp(const FP x, const FP low, const FP hi)
{
    return std::min(std::max(x, low), hi);
}

/// Smoothstep --- implementation from AMD via
/// https://en.wikipedia.org/wiki/Smoothstep
template<class FP>
FP smoothstep(const FP edge0, const FP edge1, const FP x)
{
    // Scale, bias and saturate x to 0..1 range
    const FP y = clamp((x - edge0)/(edge1 - edge0), 0.0, 1.0);
    // Evaluate polynomial
    return y*y*(3.0 - 2.0*y);
} //smoothstep

} //namespace Incline

#endif //_CONTENT_HPP_

// vi: set ts=4 sts=4 sw=4 et ai ff=dos fo=crql: //
